#dette scriptet gjør:
# - henter navn på avdelinger fra departments.csv
# - henter informasjon om selskapet fra companyinfo.csv
# - bygger OU-er for både maskiner og brukere
# - bygger paths, lagrer som strenger i departments.csv ($obj.Path)

#Bruk koden fra importCompanyInfo.ps1
$departmentsPath = "02 work in progress/00 informasjon om selskapet/departments.csv"
#Oppdater path!
$departments = Import-Csv -Path $departmentsPath -Delimiter ";"
function cleanUp($str) {
    #gjør om et strenger til noe håndterlig for f.eks OU-paths
    $str = $str.ToLower()
    $str = $str.Replace(" ", "")
    $str = $str.Replace("æ", "ae")
    $str = $str.Replace("ø", "o")
    $str = $str.Replace("å", "a")
    $str = $str.Replace("é", "e")
    $str = $str.Replace("è", "e")
    $str = $str.Replace("-", "")

    return $str
}

foreach ($department in $departments) {
    $departmentName = cleanUp($department.Name)
    #her kan man gjøre ting
    Write-Host $departmentName, $department.path
}