# tar utgangspunkt i TIM - 4. definerer grupper og undergrupper 
$vintageLiv_brukere
$vintageLiv_grupper
$vintageLiv_pcer

$topOUs = @($vintageLiv_brukere, $vintageLiv_grupper, $vintageLiv_pcer)

$avdelinger = @('styre', 'dagligLeder', 'it', 'markedsføring', 'produktutvikling',
        'hr', 'kundeservice', 'lager', 'økonomi')
        
foreach ($ou in $topOUs) {
        New-ADOrganizationalUnit $ou -Description "Top OU for VintageLiv" -ProtectedFromAccidentalDeletion:$false
        $topOU = Get-ADOrganizationalUnit -Filter * | Where-Object {$_.name -eq "$ou"}
        #Get-ADOrganizationalUnit -Filter * | Where-Object {$_.name -eq $ou} | Remove-ADOrganizationalUnit -Recursive -Confirm:$false
            foreach ($avdeling in $avdelinger) {
                New-ADOrganizationalUnit $department `
                            -Path $topOU.DistinguishedName `
                            -Description "Deparment OU for $department in topOU $topOU" `
                            -ProtectedFromAccidentalDeletion:$false
            }
        }
                            


$underIt = @('hovedAdmin', 'utvikling')

$underProduktutvikling = @('markedsanalyse', 'innovasjon')

$underLager = @('ompakking', 'sending', 'retur')

$underOkonomi = @('lønning', 'regnskapsfører', 'kontoradmin')

#Tar utgangspunkt i TIM - 4. lager grupper 

foreach ($avdeling in $avdelinger) {
        $path = Get-ADOrganizationalUnit -Filter * | 
        Where-Object { ($_.name -eq "$avdeling") `
                        -and ($_.DistinguishedName -like "OU=$avdeling,OU=$PLACEHOLDER,*") }
        New-ADGroup -Name "g_$avdeling" `
                -SamAccountName "g_$avdeling" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "g_$avdeling" `
                -Path $path.DistinguishedName `
                -Description "$avdeling group"
}

# lager undergrupper (f.eks. hovedAdmin)

#lager underOUer 
foreach ($ui in $underIt) {
        New-ADOrganizationalUnit "$ui" -ProtectedFromAccidentalDeletion:$false `
                -Path  "OU=Økonomi,DC=Vintageliv," ` #putt inn rett path, obv...
    }

#kan vel ikke lag loop her? blir ikke det da en hovedAdmin + utvikling gruppe i HVER underOU?
 #hvordan få kun hovedAdmin i kun hovedAdminOU osv?
        #lager grupper inni underOU
        foreach ($underI in $underIt) {
                $path = Get-ADOrganizationalUnit -Filter * | 
                Where-Object {($_.name -eq "$x") `
                -and ($_.DistinguishedName -like "OU=$x,OU=it,OU=$x,*")}
        New-ADGroup -Name "g_$underI" `
                -SamAccountName "g_$underI" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "g_$underI" `
                -Path $path.DistinguishedName ` #hvis jeg får det til å funke
                -Description "$underI group"
}



foreach ($up in $underProduktutvikling) {
        New-ADOrganizationalUnit "$up" -ProtectedFromAccidentalDeletion:$false `
                -Path  "OU=Økonomi,DC=Vintageliv," ` #putt inn rett path, obv...
    }

        foreach ($underProdukt in $underProduktutvikling) {
                $path = Get-ADOrganizationalUnit -Filter * | 
                Where-Object {($_.name -eq "$x") `
                -and ($_.DistinguishedName -like "OU=$x,OU=it,OU=$x,*")}
        New-ADGroup -Name "g_$underProdukt" `
                -SamAccountName "g_$underProdukt" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "g_$underProdukt" `
                -Path $path.DistinguishedName `
                -Description "$underProdukt group"
        }


 
foreach ($ul in $underLager) {
        New-ADOrganizationalUnit "$ul" -ProtectedFromAccidentalDeletion:$false `
                -Path  "OU=Økonomi,DC=Vintageliv," ` #putt inn rett path, obv...
    }

        foreach ($underLag in $underLager) {
                $path = Get-ADOrganizationalUnit -Filter * | 
                Where-Object {($_.name -eq "$x") `
                -and ($_.DistinguishedName -like "OU=$x,OU=it,OU=$x,*")}
          New-ADGroup -Name "g_$underLag" `
                -SamAccountName "g_$underLag" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "g_$underLag" `
                -Path $path.DistinguishedName `
                -Description "$underLag group"
        }



foreach ($underOko in $underOkonomi) {
        New-ADOrganizationalUnit "$underOko" -ProtectedFromAccidentalDeletion:$false `
                -Path  "OU=Økonomi,DC=Vintageliv," ` #putt inn rett path, obv...
    }

 
        foreach ($underOko in $underOkonomi) {
                $path = Get-ADOrganizationalUnit -Filter * | # må fikses så den faktisk finner rett path
                Where-Object {($_.name -eq "$x") `
                -and ($_.DistinguishedName -like "OU=$x,OU=it,OU=$x,*")}
        New-ADGroup -Name "g_$underOko" `
                -SamAccountName "g_$underOko" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "g_$underOko" `
                -Path $path.DistinguishedName `
                -Description "$underOko group"
        }


New-ADOrganizationalUnit "salg" `
            -path  "OU=Kundeservice,OU=vintagelige"


        New-ADGroup -Name "Salg" `
                -SamAccountName "Salg" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "Salg" `
                -Path  "OU=kundeservice,OU=vintageliv" 



New-ADOrganizationalUnit "personalansvarlig" `
            -path  "OU=HR,OU=vintageliv"


        New-ADGroup -Name "g_personalansvarlig" `
                -SamAccountName "g_personalansvarlig" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "g_personalansvarlig" `
                -Path  'OU=personalansvarlig,OU=HR,OU=vintageliv' `
                -Description "Personalansvarlig group" 




# lager gruppe for alle ansatte (regler som skal gjelde alle)

New-ADGroup -name "g_all_employee" `
        -SamAccountName "g_all_employee" `
        -GroupCategory Security `
        -GroupScope Global `
        -DisplayName "g_all_employee" `
        -path "OU=LearnIT_Groups,DC=core,DC=sec" `
        -Description "all employee"


