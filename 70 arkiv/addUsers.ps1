#dette scriptet gjør:
# - leser usersForGeneration.csv
# - oppretter et passord som secure string
# - sjekker tidl oppføring av navn, gjør evt endringer
# - legger til brukeren basert på $user.Path
# - sender brukernavn og passord som to smser (dette simulerer vi bare)
# - lagrer til existingUsers.csv (uten passord)
# - sletter usersForGeneration.csv

#TODO:
# - legger til brukeren basert på $user.Path
# - sletter brukere fra usersForGeneration.csv
# - håndtere eksisterende brukere
# - legge brukere i grupper??

$existingUsers = Import-Csv -Path "02 work in progress/00 informasjon om selskapet/existingUsers.csv" -Delimiter ";"
#på sikt må vi sjekke om brukeren finnes i domenet, ikke bare i en CSV fil

$newUsersPath = "02 work in progress/00 informasjon om selskapet/usersForGeneration.csv"
$dictionaryPath = "99 script fra tidligere/ordlisteFixed.csv"
#endre til faktisk path
$pwLength = 10
#endre ønsket lengde på passord

$newUsers = Import-Csv -Path $newUsersPath -Delimiter ";"
$dictionary = Import-Csv -Path $dictionaryPath -Delimiter ";"

function checkExisting($obj) {
    # dette er koden når domenet er oppe:
        
    # $samTest = Get-ADUser -Filter { SamAccountName -eq $newUser.SamAccountName }
    # $nameTest = Get-ADUser -Filter { DisplayName -eq $newUser.DisplayName }

    #i mellomtiden:
    $samTest = $existingUsers | Where-Object { $_.SamAccountName -eq $newUser.SamAccountName }
    $nameTest = $existingUsers | Where-Object { $_.DisplayName -eq $newUser.DisplayName }
    #existingUsers inneholder per nå to brukere, en med mathcende SAM og en med matchende Display, så to av brukerne vil procce
    #Edit: ikke nå lenger, men det funket før jeg genererte nye navn
    if ($samTest) {
        return $false
    }
    elseif ($nameTest) {
        return $false
    }
    else {
        return $true
    }
    #denne funksjonen tester om brukere finns allerede, enten på brukernavn eller med DisplayName
}

function genPassword($int, $dictionary) {
    $str = ""
    while ($str.Length -lt $int) {
        $index = Get-Random -Maximum ($dictionary.Length - 1)
        $str += $dictionary[$index].Word
    }
    $num = Get-Random -Maximum 9999
    $str += $num
    return $str
    #funksjonen returnerer et passord på MINSTE lengde $int, med tilfeldige ord fra den superhemmelige ordlista, og med et tilfeldig opp til 5 sifret tall på slutten
    #kunnskap om nøyaktig lengde og nøyaktig antall tall og ord osv SVEKKER sikkerheten til et passord
    #https://www.starlab.io/blog/why-enforced-password-complexity-is-worse-for-security-and-what-to-do-about-it
}

function sendText($str, $num) {
    #denne koden kan vi selvsagt ikke lage, men se for deg at vi sender strengen $str som en sms til tlf nummer $user.PhoneNumber
    #i stedet legger vi til brukernavn og passord i en fil
    #jeg gjentar: vi skal egentlig IKKE lagre brukernavn og passord i klartekst, men sender det som sms. Vi beskriver dette i rapporten.
    Write-Output "sms sendt til ($num): $str"
}

function hashString($str) {
    $stringAsStream = [System.IO.MemoryStream]::new()
    $writer = [System.IO.StreamWriter]::new($stringAsStream)
    $writer.write($str)
    $writer.Flush()
    $stringAsStream.Position = 0
    $hash = Get-FileHash -InputStream $stringAsStream
    return $hash
    #Vi kan generere sterkere/lengre hashes med et flagg, men 256 er nok for oppgavens del. Lenger hash = lenger tid å generere
}
function createUser($obj) {
    if (checkExisting($obj)) {
        #opprett ny bruker
        # Write-Output "Brukeren finns IKKE"
        # $newUser.DisplayName
        # $newUser.SamAccountName
        #kode over kun brukt for testing

        $password = genPassword $pwLength $dictionary
        $hash = (hashString $password).Hash
        #dette genererer et password basert på genPassword funksjonen, og en SHA256 hash av passordet

        sendText "Du kommer til å motta to meldinger. Den første inneholder brukernavn. Den andre inneholder passord." $obj.PhoneNumber
        sendText "Brukernavn: $($obj.SamAccountName)" $obj.PhoneNumber
        sendText "Passord: $($password)" $obj.PhoneNumber
        #sender brukernavn og passord på sms til brukeren

        #sendText gjør ingenting, derfor gjør vi koden under i stedet, men IRL ville vi IKKE lagret det som klartekst
        $databasePath = "02 work in progress/00 informasjon om selskapet/usersDatabaseFAKE.csv"
        #oppdater path!!!
        $obj | Add-Member -MemberType NoteProperty -Name Password -Value $password
        $obj | Add-Member -MemberType NoteProperty -Name Hash -Value $hash
        $obj | Export-Csv $databasePath -NoTypeInformation -Encoding 'UTF8' -Delimiter ";" -UseQuotes Never -Append -Force
        #slutt på koden vi egentlig ikke bruker

        $password = ConvertTo-SecureString $password -AsPlainText -Force
        #etter å ha sendt sms med passord sikrer vi passordet, og passer på at det aldri lagres noe sted i klartekst annet enn i minnet
        $obj | Export-Csv -Path "02 work in progress/00 informasjon om selskapet/existingUsers.csv" -Delimiter ";" -UseQuotes Never -Append
        # ^ midlertidig frem til vi har domenet legges brukeren til i en csv
        
        # New-ADUser`
        # #-WhatIf` #uncomment denne for å teste brukergenerasjon uten å faktisk gjøre det
        # -AccountPassword $securePassword`
        # -AllowReversiblePasswordEncryption $false`
        # #Dette skal være false med mindre man har store behov for tredjepartsapplikasjoner som skal bruke samme passord
        # #Kilde: https://learn.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/store-passwords-using-reversible-encryption
        # -ChangePasswordAtLogon $false`
        # #Dette vil vi egentlig ha som true, men da kan vi ikke logge inn remotely. Vi beskriver dette i rapporten.
        # -Department $obj.Department`
        #     -DisplayName $obj.DisplayName`
        #     -EmailAddress $obj.Email`
        #     #-EmployeeNumber <String>` #kanskje vi skal inkludere dette?
        #     -Enabled $true`
        # -GivenName $obj.FirstName`
        #     #-HomeDirectory <String>` #???
        #     #-Manager <ADUser>` #Dette kan vi få til hvis vi lager alle sjefene først. Format: samAccountName
        # -MobilePhone $obj.PhoneNumber`
        #     -Path $obj.Path`
        #     -SamAccountName $obj.SamAccountName`
        #     -Surname $obj.LastName`
        #     -Title $obj.Role`
        #     -UserPrincipalName $obj.UserPrincipalName
        # #Jeg vet ikke hvorfor prettier-utvidelsen velger å indente koden sånn...
    
    }
    else {
        #brukeren finnes, gjør noe annet
        Write-Output "Brukeren FINNS"
        $obj.DisplayName
        $obj.SamAccountName
        #kode over kun brukt for testing 
        #Skrive til logg
    }
}

foreach ($newUser in $newUsers) {
    createUser $newUser
}

#kilder:
#https://techcommunity.microsoft.com/t5/windows-powershell/check-if-user-already-exists/m-p/2784891
#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_logical_operators?view=powershell-7.3
#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-random?view=powershell-7.3
#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.security/convertto-securestring?view=powershell-7.3
#https://learn.microsoft.com/en-us/powershell/module/activedirectory/new-aduser?view=windowsserver2022-ps
#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-filehash?view=powershell-7.3

