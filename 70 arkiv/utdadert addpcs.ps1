#kjøres på alle maskinene som skal inn i domenet
$companyPath = "02 work in progress/00 informasjon om selskapet/companyInfo.csv"

$companyInfo = Import-Csv -Path $companyPath -Delimiter ";"
$domain = $companyInfo.DomainInternal.Split(".")
$domainName = $companyInfo.DomainInternal

$dc0 = $domain[0] #vintageliv

#### hardkodet ip for dc1 testing
$DC1IP = "192.168.111.2"

Get-NetAdapter | Set-DnsClientServerAddress -ServerAddresses $DC1IP

#definere brukernavn
$username = $dc0 + "\Administrator" 
$cred = Get-Credential -UserName $username -Message 'Cred'

#legger til maskinen i domenet
Add-Computer -Credential $cred -DomainName $domainName -PassThru -Verbose

Restart-Computer

# ######### copy paste vennlig

# #kjøres på alle maskinene som skal inn i domenet
# $domainName = "vintageliv.intern"

# $dc0 = "vintageliv"

# #### hardkodet ip for dc1 testing
# $DC1IP = "192.168.111.161"

# Get-NetAdapter | Set-DnsClientServerAddress -ServerAddresses $DC1IP

# #definere brukernavn
# $username = $dc0 + "\Administrator" 
# $cred = Get-Credential -UserName $username -Message 'Cred'

# #legger til maskinen i domenet
# Add-Computer -Credential $cred -DomainName $domainName -PassThru -Verbose

# Restart-Computer