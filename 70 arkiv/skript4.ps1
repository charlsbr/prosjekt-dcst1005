# $VintageLiv_Users = "VintageLiv_Users"
# $VintageLiv_Groups = "VintageLiv_Groups"
# $VintageLiv_Computers = "VintageLiv_Computers"

#$topOUs = @($VintageLiv_users,$VintageLiv_groups,$VintageLiv_computers)
$departments = @('styre', 'dagligLeder', 'it', 'markedsføring', 'produktutvikling',
        'hr', 'kundeservice', 'lager', 'økonomi') 

    
#Aksel fiks dette plis, takk! 
# gir tilgang til ansatte over RDP til clientmaskiner
$clientsOU = "OU=hr,OU=LearnIT_Computers,DC=Core,DC=sec"
$rdpgroup = Get-ADGroup -identity "l_remotedesktop"
$rdpgroupSID = (New-Object System.Security.Principal.SecurityIdentifier($rdpgroup.SID)).Value
# Må kjøres i PSSession på DC1 eller invoke cmd (invoke gjør det litt tricky med variableinnhold)
# Install-WindowsFeature GPMC får følgende beskjed:
# "Install-WindowsFeature: The target of the specified cmdlet cannot be a Windows client-based operating system."




###FERDIG

#Gjøres på mgr pc'n med domene-admin bruker     
#installere på server (og muligens domenekontroller) en tjeneste som samler delte filer (også hvis man har flere servere) slik at brukere kan se og interagere med delte filer. 
# dette gjør at en enkelt path leder til samme fil uavhengig av hvilken server den ligger på... lager ett navn som kan refereres til i path (hvis jeg har skjønt det rett)
#LES MER HER; https://learn.microsoft.com/en-us/windows-server/storage/dfs-namespaces/dfs-overview

Install-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con `
-ComputerName srv1 ` #Bytt ut med rett server navn hvis vår heter noe annet, usikker på hvor mye som må endres i koden under her såfall
-IncludeManagementTools



                                                ################opprette mapper for fildeling#####################

#Gjøres inne på MGR, men remoter inn på serveren og kjører opprettingen av filene (Kan dette bare gjøres direkte på srv1?)


Enter-PSSession -ComputerName srv1

## Oppretter mapper for share og DFS Root folder og delte mapper for alle de forskjellige avdelingene man skal ha, 
      ###(fikser rettigheter lenger ned slik at kun IT-ansatte har tilgang til IT-mappen osv.., men ALLE kan se mappen)
$folders = ('C:\dfsroots\files', 'C:\shares\styre', 'C:\shares\dagligLeder', 'C:\shares\it', 'C:\shares\markedsføring', 'C:\shares\produktutvikling','C:\shares\hr', 'C:\shares\økonomi', 'C:\shares\lager', 'C:\shares\kundeservice') #skal dette være bare underOUene? 
mkdir -path $folders
# Deler alle mappene så de er tilgjengelige på nettverket;
$folders | ForEach-Object { $sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone } 
                        #denne setter også rettigheter slik at ALLE får tilgang til ALT...  
                          ###TRor dette MÅ inkluderes slik at vi har rettighet til å endre rettigheter senere
                                                                                               


#################Dette MÅ gjøres i powershell 7 administrator direkte inne i srv1, kan ikke gjøres remote!! ###############

    #mulig det er bedre å bare kjøre hele fila fra srv1 i utgangspunktet?
          ## såfall må vi flytte de to foreach-loopene nedenfor (for oppretting av delte filområder pr avdeling) over til annen fil
New-DfsnRoot -TargetPath \\srv1\files -Path \\vintageliv.intern\file  -Type DomainV2 
                        #oppretter et navn i stede for path slik at det er lett for brukere å finne fram til filene inne på de forskjellige pc'ene
                           



# Benytter innholdet i $folders variabelen fra tidligere og peker alle delte områdene fra \\srv1\ til --> \\vintageliv.intern\files 

$folders | Where-Object {$_ -like "*shares*"} | 
            ForEach-Object {$name = (Get-Item $_).name; `
                $DfsPath = ('\\vintageliv.intern\files\' + $name); `
                $targetPath = ('\\srv1\' + $name);New-DfsnFolderTarget `
                -Path $dfsPath `
                -TargetPath $targetPath}





                


#################For å fikse sånn at de rette gruppene får tilgang til de rette fil-områdene!!################ 
        ###Dette må gjøres ETTER at de to foreach-løkkene over er utført!!!!!!!!!!!!


 

    foreach ($department in $departments) {    
            $acl = Get-Acl \\vintageliv\files\$department
            $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("l_fulltilgang_$department-deling","FullControl","Allow")
            $acl.SetAccessRule($AccessRule)
            $ACL | Set-Acl -Path "\\vintageliv\files\$department" 
        }
        


 #setter rette tilgangsrettigheter        
foreach ($department in $departments) {
        $ACL = Get-Acl -Path "\\vintageliv\files\$department"
        $ACL.SetAccessRuleProtection($true,$true)  #Betyr at rettighetene arves nedover i mappe-strukturen, og kan ikke endres eller slettes, skal vi endre på dette? kan gis forskjellige arv-innstillinger
        $ACL | Set-Acl -Path "\\vintageliv\files\$department" 
}


#Dette sletter tilgangen til alle som ikke skal ha tilgang til de forskjellige filene! 
    foreach ($department in $departments) {
        $acl = Get-Acl "\\vintageliv\files\$department"
        $acl.Access | Where-Object {$_.IdentityReference -eq "BUILTIN\Users" } | ForEach-Object { $acl.RemoveAccessRuleSpecific($_) }
        Set-Acl "\\vintageliv\files\$department" $acl
        (Get-ACL -Path "\\vintageliv\files\$department").Access | 
            Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize
     }
    



#### https://youtu.be/0SO81gELfU8?t=2333 her raser Tor Ivar igjennom mesteparten av koden over og forklarer veldig kjapt de forskjellige stegene! 
                                           ###(de fleste tingene han har gått igjennom er enda ikke laget til foreach løkker i videoen)

                                           
#Nevnes helt i slutten av den jævli lange leksjon5-videoen noe om å flytte pcer til rett learnIT_computers ved hjelp av "Get-ADcomputer" og | til "Move-ADObject" men ikke spesifikk kode for det.. 
#jeg er ikke flink nok til å lage kode til dette! 
   #Hvis jeg forstår det rett så setter man tilgangs-rettigheter (group policy?) på de forskjellige gruppene ala learnIT_groups, og knytter de opp mot korrensponderende LearnIT_computers mappe slik at kun de som tilhører den 
   #LearnIT_group'en får tilgang til pc'n som ligger i tilsvarende learnIT_computers'OU   (JA jeg er en smule forvirret....)



#tvinge igjennom å oppdatere rettighets-innstillingene (for hvilken pc som tilhører hvilken gruppe)
#fiikse brannmur-regler som må endres på pc'ene først for å få lov til dette!
  
# Firewall rules fiks! 

Invoke-Command -ComputerName "srv1" ` #muligens endre pc-navn
    -ScriptBlock {Enable-NetFirewallRule -Name "RemoteTask-In-TCP","WMI-WINMGMT-In-TCP","RemoteTask-RPCSS-In-TCP"}

Invoke-Command -ComputerName "cl1", "cl2", "cl3" ` #muligens endre pc-navn
    -ScriptBlock {Enable-NetFirewallRule -Name "RemoteTask-In-TCP-NoScope","WMI-WINMGMT-In-TCP-NoScope","RemoteTask-RPCSS-In-TCP-NoScope"}

Invoke-Command -ComputerName "cl1", "cl2", "cl3", "srv1" `  #muligens endre pc-navn hvis våre heter noe annet 
    -ScriptBlock {gpupdate /force}

$vms =@('cl1','cl2','cl3','srv1') #muligens endre pc-navn
foreach ($vm in $vms) {
    Invoke-GPUpdate -Computer "$vm.core.sec" -Target "User" -RandomDelayInMinutes 0 -Force ##fiks til vårt navn 
} 




###########ETTER HER HAR JEG IKKE KONTROLL (enda), YEEEEY#######


#### - Leksjon 05 - Video 3 - Installer DFS Replication på DC1 for replikering av shared folders #### Sjekk ut hva faen dette er!!!!!
# https://learn.microsoft.com/en-us/powershell/module/dfsr/?view=windowsserver2022-ps 



# Utføres på SRV1
$departments = @('styre', 'dagligLeder', 'it', 'markedsføring', 'produktutvikling',
'hr', 'kundeservice', 'lager', 'økonomi') 
foreach ($department in $departments) {
    New-DfsReplicationGroup -GroupName "RepGrp$department-Share" 
    Add-DfsrMember -GroupName "RepGrp$department-Share" -ComputerName "srv1","dc1"  #muligens endre pc-navn
    Add-DfsrConnection -GroupName "RepGrp$department-Share" `
                        -SourceComputerName "srv1" ` #muligens endre pc-navn
                        -DestinationComputerName "dc1"  #muligens endre pc-navn

    New-DfsReplicatedFolder -GroupName "RepGrp$department-Share" -FolderName "Replica$department-SharedFolder" 

    Set-DfsrMembership -GroupName "RepGrp$department-Share" `
                        -FolderName "Replica$department-SharedFolder" `
                        -ContentPath "C:\shares\$department" `
                        -ComputerName "srv1" ` #muligens endre pc-navn
                        -PrimaryMember $True 

    Set-DfsrMembership -GroupName "RepGrp$department-Share" `
                        -FolderName "Replica$department-SharedFolder" `
                        -ContentPath "c:\Replica$department-SharedFolder" `
                        -ComputerName "dc1" #muligens endre pc-navn
}
Get-DfsrCloneState 
