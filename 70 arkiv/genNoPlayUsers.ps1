$importEmployees = "02 work in progress/00 informasjon om selskapet/noPlayUsers.csv"
$companyPath = "02 work in progress/00 informasjon om selskapet/companyInfo.csv"
$employees = Import-Csv -path $importEmployees -Delimiter ";"
$companyInfo = Import-Csv -Path $companyPath -Delimiter ";"
$internalDomain = $companyInfo.DomainInternal.Split(".")
$externalDomain = $companyInfo.DomainExternal.Split(".")

function sanitizeName($string) {
    $string = $string.ToLower()
    $string = $string.Replace("æ", "ae")
    $string = $string.Replace("ø", "o")
    $string = $string.Replace("å", "a")
    $string = $string.Replace("é", "e")
    $string = $string.Replace("è", "e")
    $string = $string.Replace("-", "")
    return $string
}
function addUserParams($obj) {
    $names = $obj.DisplayName.Split(" ")
    $sam = "$($obj.FirstName)Domain"

    $userPrincipalName = $sam + "@" + $internalDomain[0] + "." + $internalDomain[1]
    $firstName = [String]::Join(' ', $names[0..($names.Length - 2)])
    $path = "FINN UT HVA PATHEN SKAL VÆRE"
    $obj | Add-Member -MemberType NoteProperty -Name FirstName -Value $firstName
    $obj | Add-Member -MemberType NoteProperty -Name LastName -Value $names[-1] #ERROR?
    $obj | Add-Member -MemberType NoteProperty -Name SamAccountName -Value $sam
    $obj | Add-Member -MemberType NoteProperty -Name UserPrincipalName -Value $userPrincipalName
    $obj | Add-Member -MemberType NoteProperty -Name Path -Value $path
}
#Kilder:
#https://java2blog.com/add-property-to-object-powershell/
#https://stackoverflow.com/questions/9298699/select-all-words-from-string-except-last-powershell
#https://www.sqlshack.com/powershell-split-a-string-into-an-array/
#https://stackoverflow.com/questions/1785474/get-index-of-current-item-in-a-powershell-loop
#https://learn.microsoft.com/en-us/powershell/scripting/learn/deep-dives/everything-about-if?view=powershell-7.3
#https://www.educba.com/powershell-loop-through-array/
#https://www.educba.com/powershell-join-array/
foreach ($employee in $employees) {
    addUserParams $employee
}

#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/export-csv?view=powershell-7.3


$existingUsers = Import-Csv -Path "02 work in progress/00 informasjon om selskapet/existingUsers.csv" -Delimiter ";"
#på sikt må vi sjekke om brukeren finnes i domenet, ikke bare i en CSV fil

function checkExisting($obj) {
    # dette er koden når domenet er oppe:
        
    # $samTest = Get-ADUser -Filter { SamAccountName -eq $newUser.SamAccountName }
    # $nameTest = Get-ADUser -Filter { DisplayName -eq $newUser.DisplayName }

    #i mellomtiden:
    $samTest = $existingUsers | Where-Object { $_.SamAccountName -eq $newUser.SamAccountName }
    $nameTest = $existingUsers | Where-Object { $_.DisplayName -eq $newUser.DisplayName }
    if ($samTest) {
        return $false
    }
    elseif ($nameTest) {
        return $false
    }
    else {
        return $true
    }
}

function genPassword($int, $dictionary) {
    $str = ""
    while ($str.Length -lt $int) {
        $index = Get-Random -Maximum ($dictionary.Length - 1)
        $str += $dictionary[$index].Word
    }
    $num = Get-Random -Maximum 9999
    $str += $num
    return $str
    #funksjonen returnerer et passord på MINSTE lengde $int, med tilfeldige ord fra den superhemmelige ordlista, og med et tilfeldig opp til 5 sifret tall på slutten
    #kunnskap om nøyaktig lengde og nøyaktig antall tall og ord osv SVEKKER sikkerheten til et passord
    #https://www.starlab.io/blog/why-enforced-password-complexity-is-worse-for-security-and-what-to-do-about-it
}

function hashString($str) {
    $stringAsStream = [System.IO.MemoryStream]::new()
    $writer = [System.IO.StreamWriter]::new($stringAsStream)
    $writer.write($str)
    $writer.Flush()
    $stringAsStream.Position = 0
    $hash = Get-FileHash -InputStream $stringAsStream
    return $hash
}
function createUser($obj) {
    if (checkExisting($obj)) {
        $password = "Sikkert.Passord123"
        $hash = (hashString $password).Hash
        $databasePath = "02 work in progress/00 informasjon om selskapet/usersDatabaseFAKE.csv"
        #oppdater path!!!
        $obj | Add-Member -MemberType NoteProperty -Name Password -Value $password
        $obj | Add-Member -MemberType NoteProperty -Name Hash -Value $hash
        $obj | Export-Csv $databasePath -NoTypeInformation -Encoding 'UTF8' -Delimiter ";" -UseQuotes Never -Append -Force

        $password = ConvertTo-SecureString $password -AsPlainText -Force
        $path = "CN=$($obj.SamAccountName),CN=Users,DC=$dc0,DC=$dc1"
        $obj | Export-Csv -Path "02 work in progress/00 informasjon om selskapet/existingUsers.csv" -Delimiter ";" -UseQuotes Never -Append
        # ^ midlertidig frem til vi har domenet legges brukeren til i en csv
        
        # New-ADUser`
        # #-WhatIf` #uncomment denne for å teste brukergenerasjon uten å faktisk gjøre det
        # -AccountPassword $securePassword`
        # -AllowReversiblePasswordEncryption $false`
        # #Dette skal være false med mindre man har store behov for tredjepartsapplikasjoner som skal bruke samme passord
        # #Kilde: https://learn.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/store-passwords-using-reversible-encryption
        # -ChangePasswordAtLogon $false`
        # #Dette vil vi egentlig ha som true, men da kan vi ikke logge inn remotely. Vi beskriver dette i rapporten.
        #     -DisplayName $obj.DisplayName`
        #     -Enabled $true`
        # -GivenName $obj.FirstName`
        #     -Path $obj.Path`
        #     -SamAccountName $obj.SamAccountName`
        #     -Surname $obj.LastName`
        #     -Title $obj.Role`
        #     -UserPrincipalName $obj.UserPrincipalName
        # #Jeg vet ikke hvorfor prettier-utvidelsen velger å indente koden sånn...
    
    }
    else {
        #brukeren finnes, gjør noe annet
        Write-Output "Brukeren FINNS"
        $obj.DisplayName
        $obj.SamAccountName
        #kode over kun brukt for testing 
        #Skrive til logg
    }
}

foreach ($newUser in $employees) {
    createUser $newUser
}

#kilder:
#https://techcommunity.microsoft.com/t5/windows-powershell/check-if-user-already-exists/m-p/2784891
#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_logical_operators?view=powershell-7.3
#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-random?view=powershell-7.3
#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.security/convertto-securestring?view=powershell-7.3
#https://learn.microsoft.com/en-us/powershell/module/activedirectory/new-aduser?view=windowsserver2022-ps
#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-filehash?view=powershell-7.3

