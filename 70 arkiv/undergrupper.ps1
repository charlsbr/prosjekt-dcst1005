#"syntax" for undergrupper 



#New-ADOrganizationalUnit ($deparment, kan dette byttes ut med bare navnet på mappa? evt @mappenavn?)
#Finner ikke ut hvordan jeg får rett path til mappen hvor det skal opprettes ny gruppe




#Looper igjennom og lager underOUer, må bare byttes ut pr kategori
foreach ($underO in $underOkonomi) {
    New-ADOrganizationalUnit "$underO" -ProtectedFromAccidentalDeletion:$false `
        -Path  "OU=$subdepartment,OU=$path" ` #fikses seinere

            
}
#ideelt hatt en til foreach loop for å legge til grupper for hver $underO! men plages med path.....

#lage ny underOU enkeltvis (der det ikke er flere under-kategorier, eks HR)
New-ADOrganizationalUnit "utviklere_undergruppe" ` #må bytte navn hver gang
-path  "OU=dev,OU=LearnIT_Groups,DC=core,DC=sec"` #må byttes ut med rett path, testet i leksjon-systemet

#lager en gruppe (hvor ansatte kan legges til, rettigheter styres osv) for eksemplet over. 

New-ADGroup -Name "utvikler" `
    -SamAccountName "utvikler" `
    -GroupCategory Security `
    -GroupScope DomainLocal `
    -DisplayName "utvikler" `
    -Path  "OU=utviklere_undergruppe,OU=dev,OU=LearnIT_Groups,DC=core,DC=sec" ` #må også byttes ut med rett path
-Description "test" 


#fjernet fra orginal under-gruppe funksjon, må fikses 
$path = Get-ADOrganizationalUnit -Filter * | 
Where-Object { ($_.name -eq "$x") `
        -and ($_.DistinguishedName -like "OU=$x,OU=it,OU=$x,*") }


#Gruppelaging inni hver underOU med KUN matchende gruppe...

    foreach ($underP in $underProdukt) {
        if ($underP == $underProdukt) {
              New-ADGroup -Name "g_$underP" `
                -SamAccountName "g_$underP" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "g_$underP" `
                -Path $path.DistinguishedName ` #vet ikke hvordan jeg skal løse path her...
                -Description "$underP group"

        } else {
            return 
        }
    }



#####################for felles fildeling fra leksjon 5 ######################### HAR ingenting med undergrupper å gjøre
Enter-PSSession -ComputerName srv1
# Oppretter mapper for share og DFS Root folder
# In DFS terms the Root is the share and a Link is a virtual Folder name to a remote server Share\folder.
$folders = ('C:\dfsroots\files', 'C:\shares\finance', 'C:\shares\sale', 'C:\shares\it', 'C:\shares\dev', 'C:\shares\hr')
mkdir -path $folders
# Deler alle mappene så de er tilgjengelige på nettverket
$folders | ForEach-Object { $sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone }