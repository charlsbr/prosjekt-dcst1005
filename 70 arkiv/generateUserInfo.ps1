#TODO:
# - lese avdeling, legge til OU path fra departments.csv

$importEmployees = "02 work in progress/00 informasjon om selskapet/testUsers3.csv"
$companyPath = "02 work in progress/00 informasjon om selskapet/companyInfo.csv"
$departmentsPath = "02 work in progress/00 informasjon om selskapet/departments.csv"
$exportPath = "02 work in progress/00 informasjon om selskapet/usersForGeneration.csv"

#må endres til faktiske paths

$employees = Import-Csv -Path $importEmployees -Delimiter ";"
$companyInfo = Import-Csv -Path $companyPath -Delimiter ";"
$departments = Import-Csv -Path $departmentsPath -Delimiter ";"
#henter informasjon om selskapet, avdelinger, og de ansatte
$company = $companyInfo.CompanyName #"VintageLiv"
#Problem: assigned but never used. Kan fjernes?
$internalDomain = $companyInfo.DomainInternal.Split(".")
$externalDomain = $companyInfo.DomainExternal.Split(".")

function sanitizeName($string) {
    $string = $string.ToLower()
    $string = $string.Replace("æ", "ae")
    $string = $string.Replace("ø", "o")
    $string = $string.Replace("å", "a")
    $string = $string.Replace("é", "e")
    $string = $string.Replace("è", "e")
    $string = $string.Replace("-", "")
    return $string
    #dette erstatter/fjerne uvanlige tegn
}
function addUserParams($obj) {
    #objektet vi starter med har fullt navn, avdeling de jobber i, rolle, og telefonnummer
    $names = $obj.DisplayName.Split(" ")

    $sam = ""
    #sam starter som en tom streng og bygges opp med logikken nedenfor
    $samFirstName = sanitizeName($names[0])
    $samLastName = sanitizeName($names[-1])
    #fornavnet og etternavnet er lette å finne
    if ($names.Length -ge 3) {
        $samMiddleString = sanitizeName([String]::Join(' ', $names[1..($names.Length - 2)]))
        $samMiddleNames = $samMiddleString.Split(" ")
    }
    else {
        $samMiddleString = ""
    }
    #lager en streng og en array for alle navn som ikke er for-/etternavn, hvis de finns
    #disse brukes i logikken nedenfor
    if (($samFirstName.Length + $samMiddleString.Length + $samLastName.Length - $samMiddleNames.Length + 1) -le 20) {
        $sam += $samFirstName + [String]::Join('', $samMiddleNames + $samLastName)
    }
    elseif (($samFirstName.Length + $samMiddleNames.Length + $samLastName.Length) -le 20) {
        $sam += $samFirstName
        foreach ($name in $samMiddleNames) {
            $sam += $name[0]
        }
        $sam += $samLastName
    }
    elseif (($samFirstName.Length + $samLastName.Length + 1 ) -le 20) {
        $sam += ($samFirstName + $samMiddleString[0] + $samLastName)
    }
    elseif (($samFirstName.Length + $samLastName.Length) -le 20) {
        $sam += ($samFirstName + $samLastName)
    }
    else {
        for ($i = 0 ; $i -lt ($samFirstName.Length + $samLastName.Length) ; $i++) {
            if ($sam.Length -lt 20) {
                if ($i -lt $samFirstName.Length) {
                    $sam += $samFirstName[$i]
                }
                else {
                    $sam += $samLastName[($i - $samFirstName.Length)]
                }
            }
        }
    }
    #dette bygger et <= 20 tegn langt sam account name
    #vi prioriterer å bruke alle navn, deretter prøver fornavn + forbokstaver i mellomnavn + etternavn,
    #deretter forbokstav i første mellomnavn, deretter fornavn + etternavn,
    #og til slutt eventuelt klipper etternavnet (og i teorien kan klippe i fornavnet hvis det er >20 tegn)
    $userPrincipalName = $sam + "@" + $internalDomain[0] + "." + $internalDomain[1]
    #dette bygger et enkel Principal Name
    $email = $sam + "@" + $externalDomain[0] + "." + $externalDomain[1]
    #en epostadresse
    $firstName = [String]::Join(' ', $names[0..($names.Length - 2)])
    #fornavnet må være en variabel før det legges til i objektet, hvis ikke får vi errors
    $department = $departments | Where-Object { $_.Name -eq $obj.Department }
    $path = $department.Path
    #$obj | Add-Member -MemberType NoteProperty -Name KOLONNENAVN -Value VERDI
    #dette er malen for å legge til properties til objektet
    $obj | Add-Member -MemberType NoteProperty -Name FirstName -Value $firstName
    $obj | Add-Member -MemberType NoteProperty -Name LastName -Value $names[-1] #ERROR
    $obj | Add-Member -MemberType NoteProperty -Name SamAccountName -Value $sam
    $obj | Add-Member -MemberType NoteProperty -Name UserPrincipalName -Value $userPrincipalName
    $obj | Add-Member -MemberType NoteProperty -Name Email -Value $email
    $obj | Add-Member -MemberType NoteProperty -Name Path -Value $path

    #legger til alle variablene vi har laget til den ansatte
}
#Kilder:
#https://java2blog.com/add-property-to-object-powershell/
#https://stackoverflow.com/questions/9298699/select-all-words-from-string-except-last-powershell
#https://www.sqlshack.com/powershell-split-a-string-into-an-array/
#https://stackoverflow.com/questions/1785474/get-index-of-current-item-in-a-powershell-loop
#https://learn.microsoft.com/en-us/powershell/scripting/learn/deep-dives/everything-about-if?view=powershell-7.3
#https://www.educba.com/powershell-loop-through-array/
#https://www.educba.com/powershell-join-array/
foreach ($employee in $employees) {
    addUserParams $employee
}

$employees | Export-Csv -Path $exportPath -NoTypeInformation -Encoding 'UTF8' -Delimiter ";" -UseQuotes Never
#https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/export-csv?view=powershell-7.3
