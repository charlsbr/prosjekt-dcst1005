Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

$companyInfo = Import-Csv -Path "99 resources/companyInfo.csv" -Delimiter ";"
$internalDomain = $companyInfo.DomainInternal.Split(".")
$dc0 = $internalDomain[0]
$dc1 = $internalDomain[1]


Invoke-Command -ComputerName "srv1" `
    -ScriptBlock { Enable-NetFirewallRule -Name "RemoteTask-In-TCP", "WMI-WINMGMT-In-TCP", "RemoteTask-RPCSS-In-TCP" }

Invoke-Command -ComputerName "cl1", "cl2", "cl3" `
    -ScriptBlock { Enable-NetFirewallRule -Name "RemoteTask-In-TCP-NoScope", "WMI-WINMGMT-In-TCP-NoScope", "RemoteTask-RPCSS-In-TCP-NoScope" }

Invoke-Command -ComputerName "cl1", "cl2", "cl3", "srv1" `
    -ScriptBlock { gpupdate /force }

$vms = @('cl1', 'cl2', 'cl3', 'srv1')
foreach ($vm in $vms) {
    Invoke-GPUpdate -Computer "$vm.$dc0.$dc1" -Target "User" -RandomDelayInMinutes 0 -Force
} 


"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3