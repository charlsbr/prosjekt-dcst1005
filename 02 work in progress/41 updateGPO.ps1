Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

$departmentsPath = "99 resources/departments.csv"
#dette henter informasjon om selskapet og avdelingene
$departments = Import-Csv -Path $departmentsPath -Delimiter ";"
function cleanUp($str) {
    #gjør om et strenger til noe håndterlig for f.eks OU-paths
    #$str = $str.ToLower()
    $str = $str.Replace(" ", "")
    $str = $str.Replace("æ", "ae")
    $str = $str.Replace("ø", "o")
    $str = $str.Replace("å", "a")
    $str = $str.Replace("é", "e")
    $str = $str.Replace("è", "e")
    $str = $str.Replace("Æ", "Ae")
    $str = $str.Replace("Ø", "O")
    $str = $str.Replace("Å", "A")
    $str = $str.Replace("É", "E")
    $str = $str.Replace("È", "e")
    $str = $str.Replace("-", "")
    return $str
}

foreach ($department in $departments) {
    $department = $department.Name.Split("-")[0]
    Write-Host $department
    $cleanName = cleanup $department
    "`n"
    Write-Host "... Creating GPO for RDP for $department ..."
    $name = "$($cleanName)_Computers"
    $path = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $name }
    $rdpgroup = Get-ADGroup -identity "l_remotedesktop_$department"
    $rdpgroupSID = (New-Object System.Security.Principal.SecurityIdentifier($rdpgroup.SID)).Value
    $GPO = New-GPO -Name "Allow Remote Desktop for $department"
    New-GPLink -Name "Allow Remote Desktop for $department" -Target $path.DistinguishedName
    Add-GPORestrictedGroup -Name "Allow Remote Desktop for employees" -GPOPath $GPO.GPOFileSysPath -GroupName "Remote Desktop Users"
    
}



"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3