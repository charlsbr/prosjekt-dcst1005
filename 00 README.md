- Scriptene:

Nedenfor følger scriptene som er brukt for å bygge organisasjonen og alt som følger med. Dette innebærer også rekkefølgen de kjøres i. Alle scriptene har kode som viser i konsollen hvilket script det er som kjører, og hvor langt i prosessen den har kommet. Nummereringen er sekvensiell men ikke kontinuerlig, da første siffer i tallet representerer hvor i prosessen det kjøres, og andre siffer er en intern rekkefølge innenfor det igjen

- Oppsett:

Disse scriptene ligger i mappen 01 scripts, og ressursene under ./99 resources.

De første scriptene her setter opp miljøet som trengs for å fortsette med oppgaven. De installerer programvare, oppretter domene og melder maskinene inn i domenet. Scriptene caller hverandre, så prosessen er å lime inn script 00 for å få programvare, så lime inn 01 for å klone git (sette inn “minnepenn”) på alle maskiner, deretter calles alle scripts som trengs på de enkelte maskiner. Etter det logger man inn på mgr med domain/administrator og kjører 21 for å bygge NoPlay-administratorer (f.eks AkselAdmin) til oss i prosjektet, og logger inn med sin egen admin-konto for å løse resten av oppgaven. Gjennomgang av dette (pluss litt mer) ligger i bonus-materiale-videoen (trykk på bildet):

[![Bonusmateriale](https://img.youtube.com/vi/1f5dCqpf48Y/0.jpg)](https://www.youtube.com/watch?v=1f5dCqpf48Y)

Hostname sjekken er hardkodet, men kunne blitt gjort dynamisk ved å hente computers.csv og lese flagg for hvilken type maskin det er mot hostnamet til maskinen. Dette ble ikke implementert pga tid.

Script 00: installSoftware

Det første scriptet starter på en helt fersk stack med installert AD. Scriptet installerer Chocolatey, PowerShell 7 og git på samtlige maskiner. Så kjøres en hostname sjekk som installerer ADDS på dc1, RSAT ADDS tools, server manager, group policy, og windows admin center på mgr, og FS-DFS-Namespace på srv1. Underveis skrives det til skjermen for å gjøre prosessen så oversiktlig som mulig. Til slutt vises det en melding om at alt er lastet ned, før den lukker powershell konsollen. På den måten vet vi at en skjerm uten powershell betyr at den er ferdig med første steg.

Script 01: gitClone

Powershell 7 må åpnes som administrator for å kjøre dette scriptet, som cloner repositoriet til gruppen og legger den inn på filstrukturen til maskinene som om det var en USB stick (visuelt) ved å calle script 10: mountUSB. Til slutt caller dette scriptet 02.

Script 02: initialSetup

Dette gjør også en hostname check. Dette må kjøres på dc1 før det kjøres på andre maskiner. Hvis dc1 så skrives IP-adressen til en fil og pushes til git (“skrives til minnepennen”). Deretter caller den script 11 og 12 som bygger NoPlay-administratorer til oss i prosjektet, og bygger domenet. Hvis ikke, så kjøres script 20, der vi gjør en git pull og henter ip-adressen fra “minnepennen”, og melder maskinen inn i domenet med DNS=dc1. Produksjonsmiljø-prompten trengs bare for oss siden det er et prosjekt, og ville ikke blitt brukt irl.

Script 03: createShortcuts

Dette er et quality of life script som kan kjøres på første login for brukere. Det lager snarveier på skrivebordet til mapper og programmer vi bruker mye. Caller også 10.

Script 10: mountUSB

Følgende script simulerer en minnepenn som ligger på skrivebordet, men er egentlig en vanlig mappe i C:

Script 11: genAdminUsers

Dette scriptet lager lokale administratorbrukere til gruppemedlemmene (NoPlay) slik at gruppen kan logge seg på uten å representere ansatte i selskapet. Grunnen til at vi ikke valgte å ha en felles administratorbruker er at det er lettere å logge hvem som gjør hva om det skulle oppstå feil eller forvirring. Høster våre navn fra noPlayUsers.csv

Script 12: buildDomain

Scriptet bygger et domene til selskapet basert på companyInfo.csv. Calles av 02 inital setup på dc1 Passord for domain administrator opprettes, men må skrives inn manuelt av sikkerhetsmessige årsaker. Parameter for «skogen» (strukturen til selskapet) defineres, og det tidligere opprettede passordet puttes inn som administrator passord. For å initiere AD må maskinen restartes, og dette skrives til konsollen med en prompt man kan svare ja eller nei på. Her opprettes også de replikerte fildelingsområdene for hver avdeling, for å slippe å logge inn på maskinen igjen senere.

Script 20: addPCToDomain

Dette scriptet må kjøres på alle maskiner som skal legges inn i domenet. Calles i 02 inital setup for andre maskiner enn dc1. Hvis det er produksjonsmiljø, hentes IP’en som ble skrevet i et tidligere script. Hvis ikke blir ip_test hentet (som er Halvors stack). Brukernavn defineres, og maskinen blir lagt til i domenet med samme type prompt som forrige script om å reboote.

Script 21: createDomainAdmins

Brukerne til prosjektgruppen blir gjort om til domene administratorer ved å filtrere på ADusers med navn som er likt eller ligner «Admin». De valgte brukerne blir da lagt til i en ny gruppe: Domain Admins.

- Gjennomføring:

På dette tidspunktet er man logget inn på sin personlige NoPlay-administrator og gjør resten av domenebyggingen. Her starter også hovedvideoen (trykk på bildet):

[![Hovedvideo](https://img.youtube.com/vi/R-tgu6WW40o/0.jpg)](https://www.youtube.com/watch?v=R-tgu6WW40o)

Disse scriptene ligger i mappen 01 scripts, og ressursene under ./99 resources.

Script 30: buildOUsAndGroups

Dette scriptet bygger toppOUer og underOUer på domenekontrolleren fra inne på mgr maskinen. ToppOUene VintageLiv_Users, VintageLiv_Computers og VintageLiv_Groups blir opprettet. Alle seksjoner får OU-er og grupper, og alle avdelinger får OU-er inni seksjons-OUen, og egne grupper. På denne måten kan vi sette regler som gjelder for alle brukere og maskiner i en seksjon, og andre (strengere eller mindre strenge) regler for avdelingene under.

![eksempel på OU-strukturen](./03%20media/ou-struktur.png)

Figur – eksempel på OU-strukturen

Alle seksjoner får en gruppe for fildelingsområdet. Det opprettes grupper for alle brukere i avdelingen som brukes til å gi tilgang til remote-desktop senere. Det opprettes all-employees gruppe, og en leder-gruppe. Ledergruppen kunne blitt brukt for egne regler og filområder for ledere, men dette implementerer vi ikke pga skopet av oppgaven. Det opprettes også et eget OU for servere.

All data i scriptet baseres på companyInfo.csv

Alle OU-er får flagget `-ProtectFromAccidentalDeletion:$false` men det er kun for vår del, ikke implementert irl.

Vi bruker en funksjon for sanitering av særnorske tegn o.l som går igjen i flere script.

```ps1
function cleanUp($str) {

#gjør om et strenger til noe håndterlig for f.eks OU-paths

#$str = $str.ToLower()

$str = $str.Replace(" ", "")

$str = $str.Replace("æ", "ae")

$str = $str.Replace("ø", "o")

$str = $str.Replace("å", "a")

$str = $str.Replace("é", "e")

$str = $str.Replace("è", "e")

$str = $str.Replace("Æ", "Ae")

$str = $str.Replace("Ø", "O")

$str = $str.Replace("Å", "A")

$str = $str.Replace("É", "E")

$str = $str.Replace("È", "e")

$str = $str.Replace("-", "")

return $str

}
```

Script 31: addUsers

Dette skriptet henter inn personene vi har generert ftil usersForGeneration, sjekker at brukeren ikke allerede har et eksisterende SamAccountName, før den så eventuelt lager det til hver bruker. Her har vi også funksjoner som bytter ut særnorske tegn, passer på å få unike kontonavn og sjekker om det allerede eksisterer en bruker med dette brukernavnet eller visningsnavet. Det sikrer også at vanlige brukere ikke får “admin” generert i navnet sitt, slik at de ikke får admin-rettigheter som de ikke skal ha senere ved et uhell hvis noen bygger en kjapp løsning.

Alle brukere får flagget `-ChangePasswordAtLogon $false` men kun for at vi kan teste dem over rdp. Dette ville vi ikke implementert irl.

Brukernavn sendes ut på sms, men skrives egentlig til en text-fil i mappen 50 NoPlay siden vi selvsagt ikke har sms-funksjonalitet.

Scriptet loggfører errors i ./99 resources, og skriver over usersForGeneration.csv med brukere som IKKE ble opprettet. Derfor kan vi manuelt endre navn på disse, og kjøre scriptet på nytt på de resterende brukerne.

Passordene som genereres får en minste-lengde, og bygges opp av ord fra en ordliste (mer om den lenger ned), tegn og flere opp til to-sifrede tall. Å ikke definere nøyaktige parametre for passordregler gjør de vanskeligere å knekke. Passordlengden er derfor vilkårlig så lenge den er over 10 tegn, med tilfeldig antall ord, tegn og tall av tilfeldig lengde. Passordlengden settes med en variabel i begynnelsen av scriptet.

Script 32: addUsersTogroup

Dette skriptet flytter alle brukere inn i den gruppen de tilhører. Disse gruppene brukes blant annet for å gi rettigheter til fildelings-områdene, slik at bare HR får tilgang til HR sitt fildelingsområde, osv. All data i scriptet er dynamisk, bortsett fra sjekken om den ansatte jobber i Hoved Admin, for å gi administratorrettigheter. Dette kunne vi bygd, med å gi hver avdeling i departments.csv et flagg om hvorvidt brukerne skal få admin eller ikke. Alle med tittel som starter med Seksjonsleder og Avdelingsleder blir lagt i leder-gruppa.

Script 33: addComputersToOUs

Dette leser fra computers.csv der vi har sagt hvor hver enkelt maskin skal plasseres. Dette leses, og servere puttes i server-OU og maskiner puttes i avdeling_Computers.

Script 40: buildFileShare

Nå logger vi av mgr og inn på srv1. Fildelingsmapper for hver seksjon blir opprettet og lagt på riktig område. Path og rettigheter blir tilpasset til avdelingene de skal tilhøre, og brukere får kun tilgang til sin egen fildelingsmappe.

Det bygges ikke filstruktur for avdelingene (under seksjonene) eller ledere, som vi har fantasert om tidligere. Dette ville vi nok fått til om vi hadde mer tid.

Det settes opp replication mot filstrukturen på dc1.

- Veien videre:

Vi kom ikke i mål med alt vi ønsket å implementere. De siste to scriptene her sliter vi for øyeblikket med å få til å funke. De ligger i mappen 02 work in progress. De funket perfekt under testing på fredag, men IT-gudene ville ikke at det skulle funke søndag kveld på en ny stack.

Script 41: updateGPO

Denne skal sette group policy slik at de rette avdelingene får tilgang til rett fildelingsområde. De GPO-relaterte cmdlet-ene nekter å kjøre uansett hvilken maskin de kjøres på, men de funket på mgr fredag, med RSAT GPO programmet installert. Vi har ikke tid til å feilsøke dette videre.

Script 42: updateFirewall

Dette skriptet skulle ha tvunget igjennom en endring av brannmurregler, slik at man får lov å tvinge igjennom en oppdatering av group policy (som blant annet settes i skript 41) på de forskjellige maskinene. Søndag kveld fikk vi ingen respons mellom maskinene, som igjen: funket på fredag. Dette ville vi også feilsøkt videre. Dette scriptet har hardkodet hostnames, men kunne blitt gjort dynamiske ved å lese computers.csv.

- Andre script og filer:

Vi har noen “NoPlay” script og filer som ikke er en del av oppgaven, men har hjulpet oss å bygge selskapet. Disse ligger i mappen 50 NoPlay.

BuildDepartmentsCSV.ps1 leser companyInfo og skriver ut departments.csv. Etter det går vi inn og manuelt endrer titler der de ble ulogiske, og legger til antall personer som jobber i hver avdeling.

NameGenerator.html leser departments.csv og lager tilfeldige navn og telefonnummer til alle. Første person ansatt i hver seksjon og avdeling blir leder. Gir oss en csv fil med brukere som simulerer en liste over nye ansatte vi kunne fått fra f.eks HR. På den måten kan addUsers og addUsersToGroups scriptene kjøres igjen senere om vi får flere ansatte.

“Lag ordliste.ps1” leser ordliste.csv som er hentet fra ordforrådet, og bearbeider den til ordlisteFixed.csv, som inneholder enkeltstående ord med stor forbokstav. Denne ordlisten må selvsagt holdes hemmelig, da det er en angrepsvektor. Vi har sett for oss å ha den på en kryptert minnepenn med kodelås. Om vi mister tilgang til listen er uinteressant, da den kan gjenbygges om vi vil. Men om noen utenforstående får tilgang er det et sikkerhetsproblem. Ordlisten er kortere enn vi ville implementert irl, på kun 700 ord.

SentTexts.txt er en liste over alle sms-ene vi har sendt ut når brukerne ble opprettet.

Mappene 70, 98 og 99 er uinteressante for andre enn oss i prosjektet.
