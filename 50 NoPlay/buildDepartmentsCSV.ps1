#Koden under her kan hente ut hver enkelt over/under/enestående avdeling, og oppdateringer mot strukturen gjøres i companyInfo.csv
#koden må kjøres fra root
$exportPath = "./01 scripts/99 resources/departments.csv"
$companyInfo = Import-Csv -Path "./01 scripts/99 resources/companyInfo.csv" -Delimiter ";"
$internalDomain = $companyInfo.DomainInternal.Split(".")
$departments = $companyInfo.Departments.Split(",") #Array av avdelinger
function buildDepartments() {
    #en midlertidig funksjon brukt for å fylle departments.csv med roller som brukes i brukergenerering
    #denne må endres for å støtte de litt mindre straight forward gruppenavnene.
    $name = $subDepartment
    $employeeTitle = "$name-arbeider"
    if ($isSection) {
        if ($hasSub) {
            $leader = "Seksjonsleder $name"
        }
        else {
            $leader = "Seksjonsleder $name"
        }
    }
    else {
        if ($hasSub) {
            $leader = "Avdelingsleder $name"
        }
        else {
            Write-Host "What? This should never procc"
        }
    }
    $path = "test"
    $departmentObj = [PSCustomObject]@{
        Name     = $name
        Leader   = $leader
        Employee = $employeeTitle
        Employed = 2
        Path     = $path
    }
    $departmentObj | Export-Csv -Path $exportPath -NoTypeInformation -Encoding 'UTF8' -Delimiter ";" -UseQuotes Never -Append -Force

}
foreach ($department in $departments) {
    $department = $department.Split("-")
    $superDepartment = ""
    $isSection = $true
    $hasSub = $false
    foreach ($subDepartment in $department) {
        if (-not ($department.Length -eq 1)) {
            $hasSub = $true
            if (-not ($department.IndexOf($subDepartment) -eq 0)) {
                $isSection = $false
            }
            else {
                $superDepartment = $subDepartment
            }
        }
        buildDepartments
    }
}
Write-Host "!!! HUSK Å OPPDATERE FILEN MED REALISTISKE ROLLER, ANTALL SOM JOBBER DER, DRIT I PATH, OG SLETT ALLE DE TOMME !!!"