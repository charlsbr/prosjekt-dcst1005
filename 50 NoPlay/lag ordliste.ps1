#dette scriptet tar en ordliste lastet ned fra UiO med spesifikke krav om hvor skjeldent brukt ordene skal være
#ordlisten bearbeides, og lager en ny ordliste vi kan velge ord fra for å lage passord
#dette scriptet, og ordlistene skal IKKE være lagret på serveren eller på noen av maskinene, men på en kryptert minnepenn/harddisk med kode/passordtilgang
#dette lar oss lage leselige passord, uten at ordlisten kan knekkes
#jeg gjentar: ordlistene og dette scriptet skal være GODT bevoktet IRL, men for oppgavens del ligger det i filstrukturen
#hvis man mister tilgang til koden/passordet for minnepennen/harddisken kan dette gjenskapes, men det skal ikke finnes ukrypterte backups

$oldWordsPath = "50 NoPlay/ordliste.csv"
$exportPath = "50 NoPlay/ordlisteFixed.csv"
$oldWords = Import-Csv $oldWordsPath -Delimiter "`t"
$newWords = @()
foreach ($obj in $oldWords) {
    $word = $obj.Word.Split(" ")
    if ($word.Length -ge 2){
        $newWord = $word[1]
    } else {
        $newWord = $word[0]
    }
    $newWords += [pscustomobject]@{
        Word = (Get-Culture).TextInfo.ToTitleCase($newWord)
    }
}
$newWords | Export-Csv $exportPath -NoTypeInformation -Encoding 'UTF8' -Delimiter ";" -UseQuotes Never
#Kilder:
#tekstlab.uio.no/ordforradet/
#https://stackoverflow.com/questions/22694582/capitalize-the-first-letter-of-each-word-in-a-filename-with-powershell
