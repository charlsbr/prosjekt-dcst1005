#Koden under her kan hente ut hver enkelt over/under/enestående avdeling, og oppdateringer mot strukturen gjøres i companyInfo.csv
$companyInfo = Import-Csv -Path "02 work in progress/00 informasjon om selskapet/companyInfo.csv" -Delimiter ";"
#Oppdater path!!!!
$companyName = $companyInfo.CompanyName #"VintageLiv"
$internalDomain = $companyInfo.DomainInternal.Split(".")
$dc0 = $internalDomain[0] #"vintageliv"
$dc1 = $internalDomain[1] #"intern"
$departments = $companyInfo.Departments.Split(",") #Array av avdelinger

$pre = "Top OU for "
$post = " of $companyName"
$users = [PSCustomObject]@{
    name        = $companyName + "_Users"
    #VintageLiv_Users
    description = $pre + "users" + $post
}
$computers = [PSCustomObject]@{
    name        = $companyName + "_Computers"
    #VintageLiv_Computers
    description = $pre + "computers" + $post
}
$groups = [PSCustomObject]@{
    name        = $companyName + "_Groups"
    #VintageLiv_Groups
    description = $pre + "groups" + $post
}
$topOUs = @($users, $computers, $groups)
#Kilde: Aksels kode, som er basert på Tor Ivars kode
#Heretter refereres over-/underavdelinger i kommentarer som hhv seksjoner og avdelinger. En uavhengig avdeling er en seksjon også.
function cleanUp($str) {
    #gjør om et strenger til noe håndterlig for f.eks OU-paths
    #$str = $str.ToLower()
    $str = $str.Replace(" ", "")
    $str = $str.Replace("æ", "ae")
    $str = $str.Replace("ø", "o")
    $str = $str.Replace("å", "a")
    $str = $str.Replace("é", "e")
    $str = $str.Replace("è", "e")
    $str = $str.Replace("Æ", "Ae")
    $str = $str.Replace("Ø", "O")
    $str = $str.Replace("Å", "A")
    $str = $str.Replace("É", "E")
    $str = $str.Replace("È", "e")
    $str = $str.Replace("-", "")
    return $str
}

function example() {
    #denne funksjonen kan kopieres til å bli hvilken som helst du vil, behold logikken
    $superDepartment = cleanUp $superDepartment
    $subDepartment = cleanUp $subDepartment
    #cleanUp rensker navnet for mellomrom om det trengs, og kan bygges ut om vi ønsker det
    if ($isSection) {
        if ($hasSub) {
            #F.eks IT, som har underavdelinger
            Write-Host "$superDepartment har underavdelinger "
        }
        else {
            #F.eks HR, som ikekr har udderavdeling
            Write-Host "$subDepartment har IKKE underavdelinger "
        }
    }
    else {
        if ($hasSub) {
            #F.eks Lønning, som er under Økonomi
            Write-Host "$subDepartment er underavdeling til $superDepartment"

        }
        else {
            Write-Host "What? This should never procc"
            #Vi skal i teorien aldri ende opp her nede, så skriver til terminal om noe fucker seg
        }
    }
}
function buildTopOUS() {
    foreach ($ou in $topOUs) {
        #New-ADOrganizationalUnit $ou.name -Description $ou.description
        Write-Host "New OU $($ou.name) $($ou.description)"
        #$topOU = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $ou.name }
    
        # foreach ($department in $departments) {
        #     New-ADOrganizationalUnit $department -Path $topOU.DistinguishedName -Description "$department OU for $topOU.Name" -ProtectedFromAccidentalDeletion:$false
        # }
    
    }
}
function companyPath() {
    #denne burde kanskje heller returnere:
    #Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $ou.name }
    return "DETTE BLIR ANNERLEDES? OU=$companyName,DC=$dc0,DC=$dc1"
}
function buildCompanyOU() {
    $path = companyPath
    #New-ADOrganizationalUnit -Path $path
    Write-Host "Building OU $path"
}
function buildOU() {
    foreach ($ou in $topOUs) {
        #New-ADOrganizationalUnit $ou.name -Description $ou.description
        Write-Host "New OU $($ou.name) $($ou.description)"
        #$topOU = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $ou.name }
    
        # foreach ($department in $departments) {
        #     New-ADOrganizationalUnit $department -Path $topOU.DistinguishedName -Description "$department OU for $topOU.Name" -ProtectedFromAccidentalDeletion:$false
        # }
    
    
        #$companyPath = "TBD"
        #denne funksjonen kan kopieres til å bli hvilken som helst du vil, behold logikken
        $superDepartment = cleanUp $superDepartment
        $subDepartment = cleanUp $subDepartment
        #cleanUp rensker navnet for mellomrom om det trengs, og kan bygges ut om vi ønsker det
        $path = ""
        if ($isSection) {
            if ($hasSub) {
                #F.eks IT, som har underavdelinger
                #Write-Host "$superDepartment har underavdelinger "
                $path += "OU=$superDepartment,"
            }
            else {
                $path += "OU=$subdepartment,"
                #F.eks HR, som ikekr har udderavdeling
                #Write-Host "$subDepartment har IKKE underavdelinger "
            }
        }
        else {
            if ($hasSub) {
                #F.eks Lønning, som er under Økonomi
                $path += "OU=$subDepartment,OU=$superDepartment,"

            }
            else {
                Write-Host "What? This should never procc"
                #Vi skal i teorien aldri ende opp her nede, så skriver til terminal om noe fucker seg
            }
        }
        $path += companyPath
        Write-Host "Building OU $path for $($ou.Name)"
        #New-ADOrganizationalUnit -Path $string
        #Prøv å ikke gjør dette

    }
}
function buildGroups() {
    #denne funksjonen bygger grupper
    $superDepartment = cleanUp $superDepartment
    $subDepartment = cleanUp $subDepartment
    #cleanUp rensker navnet for mellomrom om det trengs, og kan bygges ut om vi ønsker det
    if ($isSection) {
        if ($hasSub) {
            #F.eks IT, som har underavdelinger
            New-ADGroup -Name "g_$superDepartment" `
                -SamAccountName "g_$superDepartment" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "g_$superDepartment" `
                -Path $path.DistinguishedName ` #vet ikke hvordan jeg skal løse path her...
            -Description "$superDepartment group"

        }
        else {
            #F.eks HR, som ikke har underavdeling
            Write-Host "$subDepartment har IKKE underavdelinger "
        }
    }
    else {
        if ($hasSub) {
            #F.eks Lønning, som er under Økonomi
            New-ADGroup -Name "g_$subdepartment" `
                -SamAccountName "g_$subdepartment" `
                -GroupCategory Security `
                -GroupScope Global `
                -DisplayName "g_$subdepartment" `
                -Path $path.DistinguishedName ` #vet ikke hvordan jeg skal løse path her...
            -Description "$subdepartment group"

        }
        else {
            Write-Host "What? This should never procc"
            #Vi skal i teorien aldri ende opp her nede, så skriver til terminal om noe fucker seg
        }
    }
}

function buildDepartments() {
    #en midlertidig funksjon brukt for å fylle departments.csv med roller som brukes i brukergenerering
    #denne må endres for å støtte de litt mindre straight forward gruppenavnene.
    $name = $subDepartment
    $employeeTitle = "$name-arbeider"
    if ($isSection) {
        if ($hasSub) {
            $employeeTitle = ""
        }
        else {
            $leader = "Seksjonsleder $name"
        }
    }
    else {
        if ($hasSub) {
            $leader = "Avdelingsleder $name"
        }
        else {
            Write-Host "What? This should never procc"
        }
    }
    $departmentObj = [PSCustomObject]@{
        Name     = $name
        Leader   = $leader
        Employee = $employeeTitle
    }
    $departmentObj | Export-Csv -Path "02 work in progress/00 informasjon om selskapet/departments.csv" -NoTypeInformation -Encoding 'UTF8' -Delimiter ";" -UseQuotes Never -Append
}

buildCompanyOU
foreach ($department in $departments) {
    $department = $department.Split("-")
    $superDepartment = ""
    $isSection = $true
    $hasSub = $false
    foreach ($subDepartment in $department) {
        if (-not ($department.Length -eq 1)) {
            $hasSub = $true
            if (-not ($department.IndexOf($subDepartment) -eq 0)) {
                $isSection = $false
            }
            else {
                $superDepartment = $subDepartment
            }
        }
        #example
        buildOU
        
        #structurePath
        #disse kan fort bli en og samme funksjon kanskje også?
    }
}

#annen kode Aksel skrev som ikke brukes noe sted:
function groupBuilder($subOU, $superOU, $topOU) {
    $path = Get-ADOrganizationalUnit -Filter * | 
    Where-Object { ($_.name -eq "$subOU") `
            -and ($_.DistinguishedName -like "OU=$subOU,OU=$superOU,OU=$topOU,*") }
    New-ADGroup -Name "g_$subOUo" `
        -SamAccountName "g_$subOU" `
        -GroupCategory Security `
        -GroupScope Global `
        -DisplayName "g_$subOU" `
        -Path $path.DistinguishedName `
        -Description "$subOU group"
}
