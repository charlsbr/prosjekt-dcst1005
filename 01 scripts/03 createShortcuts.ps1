Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
"`n"
#Denne koden skriver til konsollen hvilket script som kjører

./"10 mountUsb.ps1"

$hostname = hostname
if ($hostname -eq "mgr") {
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut("$Home\Desktop\WindowsAdminCenter.lnk")
    $Shortcut.TargetPath = "C:\Program Files\Windows Admin Center\SmeDesktop.exe"
    $Shortcut.Save()
}

$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$Home\Desktop\pwsh.lnk")
$Shortcut.TargetPath = "C:\Program Files\PowerShell\7\pwsh.exe"
$Shortcut.Save()

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3