#charlotte sitt skript 4
#INNE PÅ SRV1:

Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

$companyInfo = Import-Csv -Path "99 resources/companyInfo.csv" -Delimiter ";"
$internalDomain = $companyInfo.DomainInternal.Split(".")
$dc0 = $internalDomain[0] #"vintageliv"
$dc1 = $internalDomain[1] #"intern"
$departments = $companyInfo.Departments.Split(",") #Array av avdelinger
$hostname = hostname

function cleanUp($str) {
    #gjør om et strenger til noe håndterlig for f.eks OU-paths
    #$str = $str.ToLower()
    $str = $str.Replace(" ", "")
    $str = $str.Replace("æ", "ae")
    $str = $str.Replace("ø", "o")
    $str = $str.Replace("å", "a")
    $str = $str.Replace("é", "e")
    $str = $str.Replace("è", "e")
    $str = $str.Replace("Æ", "Ae")
    $str = $str.Replace("Ø", "O")
    $str = $str.Replace("Å", "A")
    $str = $str.Replace("É", "E")
    $str = $str.Replace("È", "e")
    $str = $str.Replace("-", "")
    return $str
}

$folders = @('C:\dfsroots\files')
foreach ($department in $departments) {
    $department = cleanup $department.Split("-")[0]
    $folders += "C:\shares\$department"
}
$folders

mkdir -path $folders

# Deler alle mappene så de er tilgjengelige på nettverket;
$folders | ForEach-Object { $sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone } 
#denne setter også rettigheter slik at ALLE får tilgang til ALT...  

$targetPath = "\\$hostname\files"
$dfsnPath = "\\$dc0.$dc1\files"
New-DfsnRoot -TargetPath $targetPath -Path $dfsnPath -Type DomainV2 

$folders | Where-Object { $_ -like "*shares*" } |
ForEach-Object {
    $name = (Get-Item $_).name
    $dfsPath = ($dfsnPath + "\" + $name)
    $targetPath = ("\\" + $hostname + "\" + $name)
    New-DfsnFolderTarget -Path $dfsPath -TargetPath $targetPath
}

# New-DfsnRoot -TargetPath "\\$hostname\files" -Path "\\$dc0.$dc1\file"  -Type DomainV2 


foreach ($department in $departments) {    
    $department = cleanup $department.Split("-")[0]
    $aclPath = "\\$dc0\files\$department"
    $acl = Get-Acl $aclPath
    $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("l_fulltilgang_$department-deling", "FullControl", "Allow")
    $acl.SetAccessRule($AccessRule)
    $ACL.SetAccessRuleProtection($true, $true)  #Betyr at rettighetene arves nedover i mappe-strukturen, og kan ikke endres eller slettes.
    $ACL.Access | Where-Object { $_.IdentityReference -eq "BUILTIN\Users" } | ForEach-Object { $acl.RemoveAccessRuleSpecific($_) }
    $ACL | Set-Acl -Path $aclPath 
    Set-Acl "\\vintageliv\files\$department" $acl
        (Get-ACL -Path "\\vintageliv\files\$department").Access | Format-Table IdentityReference, FileSystemRights, AccessControlType, IsInherited, InheritanceFlags -AutoSize
}



foreach ($department in $departments) {
    $department = cleanup $department.Split("-")[0]

    New-DfsReplicationGroup -GroupName "RepGrp$department-Share" 
    Add-DfsrMember -GroupName "RepGrp$department-Share" -ComputerName "srv1", "dc1"  
    Add-DfsrConnection -GroupName "RepGrp$department-Share" `
        -SourceComputerName "srv1" `
        -DestinationComputerName "dc1"  

    New-DfsReplicatedFolder -GroupName "RepGrp$department-Share" -FolderName "Replica$department-SharedFolder" 

    Set-DfsrMembership -GroupName "RepGrp$department-Share" `
        -FolderName "Replica$department-SharedFolder" `
        -ContentPath "C:\shares\$department" `
        -ComputerName "srv1" `
        -PrimaryMember $True 

    Set-DfsrMembership -GroupName "RepGrp$department-Share" `
        -FolderName "Replica$department-SharedFolder" `
        -ContentPath "c:\Replica$department-SharedFolder" `
        -ComputerName "dc1" 
}
Get-DfsrCloneState 

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3