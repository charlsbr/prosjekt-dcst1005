Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

git pull
#dette gjør at vi garantert har riktig ip-adresse om 02 er kjørt på dc1

#kjøres på alle maskinene som skal inn i domenet
$companyPath = "99 resources/companyInfo.csv"

$companyInfo = Import-Csv -Path $companyPath -Delimiter ";"
$domain = $companyInfo.DomainInternal.Split(".")
$domainName = $companyInfo.DomainInternal
$hostname = hostname


$dc0 = $domain[0] #vintageliv
$filepath = ''
function isProd {
    "`n"
    $ans = Read-Host "NoPlay: Is this a production environment? (Y/N)"
    $ans = $ans.ToLower()
    if ($ans -eq "y") {
        $global:filepath = '99 resources/ip_prod.txt'
    }
    elseif ($ans -eq "n") {
        $global:filepath = '99 resources/ip_test.txt'
    }
    else {
        Write-Host "You must give a proper reply.'Y' for yes, 'N' for no"
        isProd
    }
}
isProd

$DC1IP = get-content $global:filepath

Get-NetAdapter | Set-DnsClientServerAddress -ServerAddresses $DC1IP

Import-Module Microsoft.PowerShell.Management -UseWindowsPowerShell
#dette må gjøres for å ikke hoppe inn i et annet shell

#definere brukernavn
$username = $dc0 + "\Administrator" 
$cred = Get-Credential -UserName $username

#legger til maskinen i domenet
Add-Computer -Credential $cred -DomainName $domainName -PassThru -Verbose

"`n"
Write-Host "... $hostname added to domain $domainName ..."
Write-Host "... $hostname needs to reboot into $domainName. After this you will have to log in as Administrator@$domainName ..."
# prompter bruker om de vil reboote maskinen
$confirmation = Read-Host "Reboot? (Y)"
if (($confirmation -eq "Y") -Or ($confirmation -eq "y")) {
    "`n"
    Write-Host "... Rebooting ..."
    Restart-Computer -Force
}
else {
    "`n"
    Write-Host "... You will have to reboot later ..."
}

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3