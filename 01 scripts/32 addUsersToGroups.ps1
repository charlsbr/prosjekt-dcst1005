Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."

$departmentsPath = "99 resources/departments.csv"
#dette henter informasjon om selskapet og avdelingene
$departments = Import-Csv -Path $departmentsPath -Delimiter ";"

function cleanUp($str) {
    #gjør om et strenger til noe håndterlig for f.eks OU-paths
    #$str = $str.ToLower()
    $str = $str.Replace(" ", "")
    $str = $str.Replace("æ", "ae")
    $str = $str.Replace("ø", "o")
    $str = $str.Replace("å", "a")
    $str = $str.Replace("é", "e")
    $str = $str.Replace("è", "e")
    $str = $str.Replace("Æ", "Ae")
    $str = $str.Replace("Ø", "O")
    $str = $str.Replace("Å", "A")
    $str = $str.Replace("É", "E")
    $str = $str.Replace("È", "e")
    $str = $str.Replace("-", "")
    return $str
}

$ADUsers = @()

foreach ($department in $departments) {
    $department = $department.Name
    $ADUsers = Get-ADUser -Filter { Department -eq $department } -Properties Department, Title, DisplayName
    foreach ($aduser in $ADUsers) {
        $department = cleanUp $department
        "`n"
        Write-Host "... adding $($aduser.SamAccountName) to g_$department" ...
        Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "g_$department"
        Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "l_fulltilgang_$department-deling"
        Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "g_all_employees"
        $leaderTest = $aduser.Title.Split(" ")[0]
        if (($leaderTest -eq "Avdelingsleder") -or ($leaderTest -eq "Seksjonsleder")) {
            "`n"
            Write-Host "... adding $($aduser.DisplayName) to leader group ..."
            Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "g_all_leaders"
        }
        if ($aduser.Department -eq "Hoved Admin") {
            "`n"
            Write-Host "... adding $($aduser.DisplayName) to admin group ..."
            Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "Administrators"
            Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "Domain Admins"
        }
    }
}

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3   