Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

#dette scriptet bygger et domene basert på companyInfo.csv
$companyPath = "99 resources/companyInfo.csv"

$companyInfo = Import-Csv -Path $companyPath -Delimiter ";"
$dc0 = $companyInfo.DomainInternal.Split(".")[0]
$domainName = $companyInfo.DomainInternal

"`n"
Write-Host "... Building replica folders ..."

$departmentsSource = $companyInfo.Departments.Split(",")
$departments = @()
foreach ($department in $departmentsSource) {
    $departments += $department.Split("-")[0]
}

foreach ($department in $departments) {
    $folder = ("C:\Replica$department-SharedFolder")
    mkdir -path $folder
    "`n"
    Write-Host "... $folder ..."
}

"`n"
Write-Host "... Setting domain parameters ..."
"`n"

## lager et nytt passord
$Password = Read-Host -Prompt 'Create a password for user "Administrator"' -AsSecureString
#NoPlay: Skriv inn passord "Sikkert.Passord123"
#Siden vi ikke har lyst å hardkode et passord

# setter dette passored som passord for admin bruker, "må følge passord restriksjoner"
Set-LocalUser -Password $Password Administrator -Description "Domain global Administrator"

# definerer parametere for skogen
$Parameters = @{
    DomainMode                    = 'WinThreshold'
    DomainName                    = $domainName
    DomainNetbiosName             = $dc0
    ForestMode                    = 'WinThreshold'
    InstallDns                    = $true
    NoRebootOnCompletion          = $true
    SafeModeAdministratorPassword = $Password
    Force                         = $true
}

Install-ADDSForest @Parameters

#restarter for å initiere AD
"`n"
Write-Host "... ADDS forest won't initiate before reboot. After this you will have to log in as Administrator@$domainName ..."
$confirmation = Read-Host "Reboot? (Y)"
if (($confirmation -eq "Y") -Or ($confirmation -eq "y")) {
    "`n"
    Write-Host "... Rebooting ..."
    Restart-Computer -Force
}
else {
    "`n"
    Write-Host "... Forest not initiated. Reboot later to initiate. ..."
}

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3
