Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$Home\Desktop\Minnepenn.lnk")
$Shortcut.TargetPath = "C:\Git\prosjekt-dcst1005\01 scripts"
$Shortcut.Save()
#dette simulerer en minnepenn som ligger på skrivebordet
#det er liksom der vi har alle filene våre når vi kjører scriptene
#de er egentlig mounted til C: så alle brukere får tilgang til filene

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3