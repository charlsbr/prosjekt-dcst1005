Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

$importEmployees = "99 resources/usersForGeneration.csv"
#dette representerer f.eks en fil fra HR med navn, avdeling, rolle og telefonnummer til nye ansatte

$companyPath = "99 resources/companyInfo.csv"
#dette henter informasjon om selskapet og avdelingene

$employees = Import-Csv -Path $importEmployees -Delimiter ";"
$companyInfo = Import-Csv -Path $companyPath -Delimiter ";"

$internalDomain = $companyInfo.DomainInternal.Split(".")
$externalDomain = $companyInfo.DomainExternal.Split(".")

function sanitizeName($string) {
    $string = $string.ToLower()
    $string = $string.Replace("æ", "ae")
    $string = $string.Replace("ø", "o")
    $string = $string.Replace("å", "a")
    $string = $string.Replace("é", "e")
    $string = $string.Replace("è", "e")
    $string = $string.Replace("-", "")
    return $string
    #dette erstatter/fjerne uvanlige tegn
}

function cleanUp($str) {
    #gjør om et strenger til noe håndterlig for f.eks OU-paths
    #$str = $str.ToLower()
    $str = $str.Replace(" ", "")
    $str = $str.Replace("æ", "ae")
    $str = $str.Replace("ø", "o")
    $str = $str.Replace("å", "a")
    $str = $str.Replace("é", "e")
    $str = $str.Replace("è", "e")
    $str = $str.Replace("Æ", "Ae")
    $str = $str.Replace("Ø", "O")
    $str = $str.Replace("Å", "A")
    $str = $str.Replace("É", "E")
    $str = $str.Replace("È", "e")
    $str = $str.Replace("-", "")
    return $str
}
function addUserParams($obj) {
    $hasParams = [bool]($obj.PSobject.Properties.name -match "SamAccountName")
    if (-not $hasParams) {
        #Kilde:
        #https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_logical_operators?view=powershell-7.3
        #objektet vi skal starte med har kun fullt navn, avdeling de jobber i, rolle, og telefonnummer
        #hvis de allerede har SamAccountName har denne funksjonen allerede kjørt og vi lager ikke ny
        $names = $obj.DisplayName.Split(" ")
        #Kilde:
        #https://www.sqlshack.com/powershell-split-a-string-into-an-array/

        $sam = ""
        #sam starter som en tom streng og bygges opp med logikken nedenfor
        $samFirstName = sanitizeName($names[0])
        $samLastName = sanitizeName($names[-1])
        #fornavnet og etternavnet er lette å finne
        if ($names.Length -ge 3) {
            $samMiddleString = sanitizeName([String]::Join(' ', $names[1..($names.Length - 2)]))
            #Kilde:
            #https://stackoverflow.com/questions/9298699/select-all-words-from-string-except-last-powershell
            #https://www.educba.com/powershell-join-array/
            $samMiddleNames = $samMiddleString.Split(" ")
        }
        else {
            $samMiddleString = ""
        }
        #lager en streng og en array for alle navn som ikke er for-/etternavn, hvis de finns
        #disse brukes i logikken nedenfor
        if (($samFirstName.Length + $samMiddleString.Length + $samLastName.Length - $samMiddleNames.Length + 1) -le 20) {
            $sam += $samFirstName + [String]::Join('', $samMiddleNames + $samLastName)
        }
        elseif (($samFirstName.Length + $samMiddleNames.Length + $samLastName.Length) -le 20) {
            $sam += $samFirstName
            foreach ($name in $samMiddleNames) {
                $sam += $name[0]
            }
            $sam += $samLastName
        }
        elseif (($samFirstName.Length + $samLastName.Length + 1 ) -le 20) {
            $sam += ($samFirstName + $samMiddleString[0] + $samLastName)
        }
        elseif (($samFirstName.Length + $samLastName.Length) -le 20) {
            $sam += ($samFirstName + $samLastName)
        }
        else {
            for ($i = 0 ; $i -lt ($samFirstName.Length + $samLastName.Length) ; $i++) {
                if ($sam.Length -lt 20) {
                    if ($i -lt $samFirstName.Length) {
                        $sam += $samFirstName[$i]
                    }
                    else {
                        $sam += $samLastName[($i - $samFirstName.Length)]
                    }
                }
            }
        }
        #dette bygger et <= 20 tegn langt sam account name
        #vi prioriterer å bruke alle navn, deretter prøver fornavn + forbokstaver i mellomnavn + etternavn,
        #deretter forbokstav i første mellomnavn, deretter fornavn + etternavn,
        #og til slutt eventuelt klipper etternavnet (og i teorien kan klippe i fornavnet hvis det er >20 tegn)
        #dette gir lav sannsynlighet for brukere med identiske brukernavn
        
        if ($sam -clike "*admin*") { 
            Write-Host "!!! Username $sam contains the word 'admin' and will cause issues ..."
            Write-Host "... Please choose a different SamAccountName for $($obj.DisplayName) ..."
            $sam = Read-Host "Different username"
        }

        $userPrincipalName = $sam + "@" + $internalDomain[0] + "." + $internalDomain[1]
        $email = $sam + "@" + $externalDomain[0] + "." + $externalDomain[1]
        $firstName = [String]::Join(' ', $names[0..($names.Length - 2)])
        #fornavnet må være en streng før det legges til i objektet, hvis ikke får vi errors
        $department = cleanup $obj.Department
        $department += "_Users"
        $path = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $department }
        $path = $path.DistinguishedName

        #$obj | Add-Member -MemberType NoteProperty -Name KOLONNENAVN -Value VERDI
        #dette er malen for å legge til properties til objektet
        $obj | Add-Member -MemberType NoteProperty -Name FirstName -Value $firstName
        $obj | Add-Member -MemberType NoteProperty -Name LastName -Value $names[-1] #ERROR
        $obj | Add-Member -MemberType NoteProperty -Name SamAccountName -Value $sam
        $obj | Add-Member -MemberType NoteProperty -Name UserPrincipalName -Value $userPrincipalName
        $obj | Add-Member -MemberType NoteProperty -Name Email -Value $email
        $obj | Add-Member -MemberType NoteProperty -Name Path -Value $path
        #legger til alle variablene vi har laget til den ansatte
        #Kilde:
        #https://java2blog.com/add-property-to-object-powershell/
    }
    else {
        "`n"
        Write-Host "... $($obj.DisplayName) already has other params ($($obj.SamAccountName)) ..."
    }
}

foreach ($employee in $employees) {
    addUserParams $employee
}
$newCsv = "DisplayName;Department;Role;PhoneNumber;FirstName;LastName;SamAccountName;UserPrincipalName;Email;Path"
$newCsv | Out-File -FilePath $importEmployees
#overskriver listen over brukere, fylles opp igjen senere av alle brukere som feilet

$dictionaryPath = "../50 NoPlay/ordlisteFixed.csv"
#Denne pathen reflekterer en annen TRYGG minnepinne
$pwLength = 10
#sett ønsket minste lengde på passord
$dictionary = Import-Csv -Path $dictionaryPath -Delimiter ";"

function checkExisting($obj) {
    $samTest = Get-ADUser -Filter * | Where-Object { $_.SamAccountName -eq $obj.SamAccountName }
    $nameTest = Get-ADUser -Filter * | Where-Object { $_.DisplayName -eq $obj.DisplayName }
    #Kilde:
    #https://techcommunity.microsoft.com/t5/windows-powershell/check-if-user-already-exists/m-p/2784891

    $bool = $true

    if ($samTest) {
        $bool = $false
    }
    elseif ($nameTest) {
        $bool = $false
    }
    return $bool
    #denne funksjonen tester om brukere finns allerede, enten på brukernavn eller med DisplayName, skriver en error
}

function genPassword($int, $dictionary) {
    $str = ""
    #fiks tilfeldig tegn
    while ($str.Length -lt $int) {
        $index = Get-Random -Maximum ($dictionary.Length - 1)
        $str += $dictionary[$index].Word
        $str += Get-Random -Maximum 99
        $str += Get-Random -InputObject "!", "@", "#", "$", ".", ",", ":", ";"
        #Kilde:
        #https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-random?view=powershell-7.3
    }
    return $str
    #funksjonen returnerer et passord på MINSTE lengde $int, med tilfeldige ord fra den superhemmelige ordlista, og med et tilfeldig opp til 5 sifret tall på slutten
    #kunnskap om nøyaktig lengde og nøyaktig antall tall og ord osv SVEKKER sikkerheten til et passord
    #Kilde:
    #https://www.starlab.io/blog/why-enforced-password-complexity-is-worse-for-security-and-what-to-do-about-it
}

function sendText($str, $num) {
    #denne koden kan vi selvsagt ikke lage, men se for deg at vi sender strengen $str som en sms til tlf nummer $user.PhoneNumber
    #i stedet legger vi til brukernavn og passord i en fil
    #jeg gjentar: vi skal egentlig IKKE lagre brukernavn og passord i klartekst, men sender det som sms. Vi beskriver dette i rapporten.
    $msg = "Text message sent to ($num): $str"
    $filepath = '../50 NoPlay/sentTexts.txt'
    $msg | Out-File -FilePath $filepath -Append
}

function hashString($str) {
    $stringAsStream = [System.IO.MemoryStream]::new()
    $writer = [System.IO.StreamWriter]::new($stringAsStream)
    $writer.write($str)
    $writer.Flush()
    $stringAsStream.Position = 0
    $hash = Get-FileHash -InputStream $stringAsStream
    return $hash
    #Vi kan generere sterkere/lengre hashes med et flagg, men 256 er nok for oppgavens del. Lenger hash = lenger tid å generere
    #Kilde:
    #https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-filehash?view=powershell-7.3
}
function createUser($obj) {
    "`n"
    Write-Host "... Adding user $($obj.SamAccountName) for $($obj.DisplayName) ..."

    $password = genPassword $pwLength $dictionary
    #$password = "Sikkert.Passord123"
    $hash = (hashString $password).Hash
    #dette genererer et password basert på genPassword funksjonen, og en SHA256 hash av passordet

    sendText "Du kommer til å motta to meldinger. Den første inneholder brukernavn. Den andre inneholder passord." $obj.PhoneNumber
    sendText "Brukernavn: $($obj.SamAccountName)" $obj.PhoneNumber
    sendText "Passord: $($password)" $obj.PhoneNumber
    #sender brukernavn og passord på sms til brukeren

    #sendText gjør ingenting, derfor gjør vi koden under i stedet, men IRL ville vi IKKE lagret det som klartekst
    $databasePath = "../50 NoPlay/generatedUsernamesPassword.csv"
    $obj | Add-Member -MemberType NoteProperty -Name Password -Value $password
    $obj | Add-Member -MemberType NoteProperty -Name Hash -Value $hash
    $obj | Export-Csv $databasePath -NoTypeInformation -Encoding 'UTF8' -Delimiter ";" -UseQuotes Never -Append -Force
    $obj.PSObject.Properties.Remove("Password")
    #slutt på koden vi egentlig ikke bruker

    $password = ConvertTo-SecureString $password -AsPlainText -Force
    #etter å ha sendt sms med passord sikrer vi passordet, og passer på at det aldri lagres noe sted i klartekst annet enn i minnet
    #Kilde:
    #https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.security/convertto-securestring?view=powershell-7.3

    New-ADUser -Name $obj.SamAccountName -AccountPassword $password -AllowReversiblePasswordEncryption $false -ChangePasswordAtLogon $false -Department $obj.Department -DisplayName $obj.DisplayName -EmailAddress $obj.Email -Enabled $true -GivenName $obj.FirstName -MobilePhone $obj.PhoneNumber -Path $obj.Path -SamAccountName $obj.SamAccountName -Surname $obj.LastName -Title $obj.Role -UserPrincipalName $obj.UserPrincipalName
    #Flagget for change password on logon skulle være på, men det lar oss ikke teste brukerne over rdp
    #Flagget -AllowReversibleEncryption er av fordi vi ikke har programvare som trenger dette. Vi lager en hash av passordet og kan eventuelt bruke det til applikasjoner vi bygger i selskapet som ønsker å bruke brukernavn og passord.
    #Kilde:
    #https://learn.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/store-passwords-using-reversible-encryption
    #https://learn.microsoft.com/en-us/powershell/module/activedirectory/new-aduser?view=windowsserver2022-ps
}

foreach ($newUser in $employees) {
    "`n"
    Write-Host "... Checking if user for $($obj.DisplayName) is already created ..."

    if (checkExisting $newUser -eq $true) {
        createUser $newUser
    }
    else {
        $newUser | Export-Csv -Path $importEmployees -NoTypeInformation -Encoding 'UTF8' -Delimiter ";" -UseQuotes Never -Append -Force
        #Kilde:
        #https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/export-csv?view=powershell-7.3
        $msg = "!!!ERROR: Already exists: $($newUser.SamAccountName)."
        $filepath = '99 resources/00 Error logs/userAddErrors.txt'
        $msg | Out-File -FilePath $filepath -Append
        "`n"
        Write-Host $msg
    }
}

$filepath = '99 resources/00 Error logs/userAddErrors.txt'
$errorLog = Get-Content $filepath
if ($errorLog.Length -ne 0) {
    "`n"
    Write-Host "!!! Check the error log for any users thet were not created, and handle the situation accordingly ..."
    "`n"
    Write-Host "Errors:"
    Write-Host $errorLog
}

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3   