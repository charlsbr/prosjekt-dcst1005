#kopier inn i powershell som administrator

clear
"`n"
Write-Host "... Installing Chocolatey ..."

Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco upgrade chocolatey
#henter choco

"`n"
Write-Host "... Installing pwsh ..."
"`n"

choco install -y powershell-core
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$Home\Desktop\pwsh.lnk")
$Shortcut.TargetPath = "C:\Program Files\PowerShell\7\pwsh.exe"
$Shortcut.Save()
#installerer pwsh med choco

"`n"
Write-Host "... Installing git ..."
"`n"

choco install -y git.install
#installerer git med choco

$hostname = hostname
if ($hostname -eq "dc1") {
    "`n"
    Write-Host "... Installing ADDS ..."
    "`n"


    Install-WindowsFeature AD-Domain-Services, DNS -IncludeManagementTools
    "`n"
    Write-Host "... Installing FS-DFS-Replication ..."
    "`n"

    Install-WindowsFeature -name FS-DFS-Replication -IncludeManagementTools -ComputerName dc1

    #installerer ADDS på dc1
}
elseif ($hostname -eq "mgr") {
    "`n"
    Write-Host "... Installing RSAT ADDS tools, server manager and group policy manager ..."
    "`n"

    Add-WindowsCapability -online -Name Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0
    Add-WindowsCapability -Online -Name Rsat.GroupPolicy.Management.Tools~~~~0.0.1.0

    "`n"
    Write-Host "... Downloading and installing Windows Admin Center ..."
    "`n"

    Invoke-WebRequest 'https://aka.ms/WACDownload' -OutFile "$pwd\WAC.msi"
    $msiArgs = @("/i", "$pwd\WAC.msi", "/quiet", "/qn", "/L*v", "log.txt", "SME_PORT=6516", "SSL_CERTIFICATE_OPTION=generate")
    Start-Process msiexec.exe -Wait -ArgumentList $msiArgs
    #installerer RSAT ADDS tools og server manager, admin center på mgr
    #kilder:
    #https://woshub.com/install-rsat-feature-windows-10-powershell/
    #https://adamtheautomator.com/download-windows-admin-center/
}
elseif ($hostname -eq "srv1") {
    "`n"
    Write-Host "... Installing FS-DFS-Namespace ..."
    "`n"

    Install-WindowsFeature -Name FS-DFS-Namespace, FS-DFS-Replication, RSAT-DFS-Mgmt-Con -ComputerName srv1 -IncludeManagementTools
}
#sjekker hvilken maskin vi er på, og handler deretter

"`n"
Write-Host "... Done with initial software downloads and configuration ..."
"`n"

Start-Sleep -Seconds 0.3
exit
