#FJERN HARDKODING FOR DEN ENE GRUPPA PÅ SLUTTEN DER

#build all top ous and under ous
#dette kjøres inne i mgr logget inn som vår admin bruker

# hva som skal skje:
# 
    
Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

$companyInfo = Import-Csv -Path "99 resources/companyInfo.csv" -Delimiter ";"
$companyName = $companyInfo.CompanyName #"VintageLiv"
$internalDomain = $companyInfo.DomainInternal.Split(".")
$departments = $companyInfo.Departments.Split(",") #Array av avdelinger

$pre = "Top OU for "
$post = " of $companyName"
$users = [PSCustomObject]@{
    name        = $companyName + "_Users"
    #VintageLiv_Users
    description = $pre + "users" + $post
}
$computers = [PSCustomObject]@{
    name        = $companyName + "_Computers"
    #VintageLiv_Computers
    description = $pre + "computers" + $post
}
$groups = [PSCustomObject]@{
    name        = $companyName + "_Groups"
    #VintageLiv_Groups
    description = $pre + "groups" + $post
}
$topOUs = @($users, $computers, $groups)
#Kilde: Aksels kode, som er basert på Tor Ivars kode
#Heretter refereres over-/underavdelinger i kommentarer som hhv seksjoner og avdelinger. En uavhengig avdeling er en seksjon også.

function cleanUp($str) {
    #gjør om et strenger til noe håndterlig for f.eks OU-paths
    #$str = $str.ToLower()
    $str = $str.Replace(" ", "")
    $str = $str.Replace("æ", "ae")
    $str = $str.Replace("ø", "o")
    $str = $str.Replace("å", "a")
    $str = $str.Replace("é", "e")
    $str = $str.Replace("è", "e")
    $str = $str.Replace("Æ", "Ae")
    $str = $str.Replace("Ø", "O")
    $str = $str.Replace("Å", "A")
    $str = $str.Replace("É", "E")
    $str = $str.Replace("È", "e")
    $str = $str.Replace("-", "")
    return $str
}

function resetOUs() {
    foreach ($topOU in $topOUs) {
        $topOU = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $topOU.name }
        Remove-ADOrganizationalUnit $topOU -Recursive
    }
}
#denne funksjonen brukes for testing

foreach ($topOU in $topOUs) {
    $type = $topOU.Name.Split("_")[1]

    "`n"
    Write-Host "... Creating top-$type-OU for $($topOU.Name) ..."
    #variabelen $topOU er nå et objekt
    New-ADOrganizationalUnit $topOU.name -Description $topOU.description -ProtectedFromAccidentalDeletion:$false
    
    $topOU = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $topOU.name }
    #bygger top OU og gjør variabelen til OU-en som ble bygd
    if ($type -eq "Computers") {
        "`n"
        Write-Host "... Creating OU for servers ..."

        New-ADOrganizationalUnit "Servers" `
            -Description "OU for Servers" `
            -Path $topOU.DistinguishedName `
            -ProtectedFromAccidentalDeletion:$false
        $path = Get-ADOrganizationalUnit -filter * | Where-Object { $_.Name -eq "Servers" }
        New-ADGroup -GroupCategory Security `
            -GroupScope DomainLocal `
            -Name "g_Servers" `
            -Path $path.DistinguishedName `
            -SamAccountName 'g_servers'
    }


    foreach ($department in $departments) {
        $department = $department.Split("-")
        $superDepartment = ""
        $isSection = $true
        $hasSub = $false
        foreach ($subDepartment in $department) {
            "`n"
            Write-Host "... Creating $type-OU for $subdepartment ..."
            if (-not ($department.Length -eq 1)) {
                $hasSub = $true
                if (-not ($department.IndexOf($subDepartment) -eq 0)) {
                    #kilde:
                    #https://stackoverflow.com/questions/1785474/get-index-of-current-item-in-a-powershell-loop
                    $isSection = $false
                }
                else {
                    $superDepartment = $subDepartment
                }
            }
            $superName = cleanUp $superDepartment
            $subName = cleanUp $subDepartment
            #cleanUp rensker navnet for mellomrom om det trengs, og kan bygges ut om vi ønsker det
            if ($isSection) {
                #Kilde:
                #https://learn.microsoft.com/en-us/powershell/scripting/learn/deep-dives/everything-about-if?view=powershell-7.3
                if ($hasSub) {
                    #F.eks IT, som har underavdelinger
                    #Write-Host "$superDepartment har underavdelinger"
                    $name = $subName + "_" + $type
                    New-ADOrganizationalUnit $name -Path $topOU.DistinguishedName -Description "Section OU for $superDepartment in topOU $topOU" -ProtectedFromAccidentalDeletion:$false
                    
                }
                else {
                    #F.eks HR, som ikke har underavdeling
                    #Write-Host "$subDepartment har IKKE underavdelinger "
                    $name = $subName + "_" + $type
                    New-ADOrganizationalUnit $name -Path $topOU.DistinguishedName -Description "Section OU for $subDepartment in topOU $topOU" -ProtectedFromAccidentalDeletion:$false
                    
                }
            }
            else {
                if ($hasSub) {
                    #F.eks Lønning, som er under Økonomi
                    #Write-Host "$subDepartment er underavdeling til $superDepartment"
                    $name = $subName + "_" + $type
                    $superName += "_$type"
                    $superOU = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $superName }
                    $path = Out-String -InputObject $superOU.DistinguishedName
                    New-ADOrganizationalUnit $name -Path $path -Description "Department OU for $subDepartment in section OU $superDepartment, in topOU $topOU" -ProtectedFromAccidentalDeletion:$false
                    
        
                }
                else {
                    Write-Host "What? This should never procc"
                    #Vi skal i teorien aldri ende opp her nede, så skriver til terminal om noe fucker seg
                    #vi kan også skrive en error log for dette, men fuck it
                }
            }
            if ($type -eq "Groups") {
                "`n"
                Write-Host "... Creating groups for $subDepartment ..."
                $name = "$($subName)_Groups"
                $path = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $name }
                New-ADGroup -Name "g_$subName" -SamAccountName "g_$subName" -GroupCategory Security  -GroupScope Global -DisplayName "g_$subName" -Path $path.DistinguishedName -Description "$subDepartment global group"
                New-ADGroup  -Name "l_fulltilgang_$subName-deling" `
                    -SamAccountName "l_fulltilgang_$subName-deling" `
                    -GroupCategory Security `
                    -GroupScope Global `
                    -DisplayName "l_fulltilgang_$subName-deling" `
                    -Path $path.DistinguishedName `
                    -Description "$subDepartment Fildelings gruppe"
                Add-ADPrincipalGroupMembership -Identity "g_$subName" -MemberOf "l_fulltilgang_$subName-deling"
                "`n"
                Write-Host "... Creating RD group for $subName ..."
                New-ADGroup -GroupCategory Security `
                    -GroupScope DomainLocal `
                    -Name "l_remotedesktop_$subName" `
                    -Path $topOU.DistinguishedName `
                    -SamAccountName "l_remotedesktop_$subName"
            }
        }
    }
}

"`n"
Write-Host "... Creating group for all employees ..."

$name = $companyName + "_Groups"
$path = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $name }
New-ADGroup -name "g_all_employees" -SamAccountName "g_all_employees" -GroupCategory Security -GroupScope Global -DisplayName "g_all_employees" -path $path.DistinguishedName -Description "all employees at $companyName"

"`n"
Write-Host "... Creating group for all leaders ..."

$name = $companyName + "_Groups"
$path = Get-ADOrganizationalUnit -Filter * | Where-Object { $_.name -eq $name }
New-ADGroup -name "g_all_leaders" -SamAccountName "g_all_leaders" -GroupCategory Security -GroupScope Global -DisplayName "g_all_leaders" -path $path.DistinguishedName -Description "all leaders at $companyName"

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3