Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
"`n"
#Denne koden skriver til konsollen hvilket script som kjører

$hostname = hostname
function addIp() {
    "`n"
    $ans = Read-Host "NoPlay: Is this a production environment? (Y/N)"
    $ans = $ans.ToLower()
    if ($ans -eq "y") {
        $global:filepath = '99 resources/ip_prod.txt'
    }
    elseif ($ans -eq "n") {
        $global:filepath = '99 resources/ip_test.txt'
    }
    else {
        Write-Host "You must give a proper reply.'Y' for yes, 'N' for no"
        addIp
    }
}
if ($hostname -eq "dc1") {
    addIp

    "`n"
    Write-Host "... Writing IP configuration to 'USB' (Git) ..."
    "`n"

    $ip = (Get-NetIpConfiguration).IPv4Address.IPAddress
    #lagrer ipaddresssen i en tekstfil, slik at denne kan brukes når vi legger til de andre maskinene i domenet
    #$filepath = '99 resources/ip.txt'
    $ip | Out-File -FilePath $global:filepath
    git diff
    git add $global:filepath
    git commit -m "adding the new IP for DC1"
    git push
    #NoPlay: her må vi ha en token til gitlab å kopiere inn
    ./"11 genAdminUsers.ps1"
    ./"12 buildDomain.ps1"
}
else {
    ./"20 addPCToDomain.ps1"
}
