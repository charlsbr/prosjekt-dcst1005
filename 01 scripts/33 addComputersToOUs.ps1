Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

$companyInfo = Import-Csv -Path "99 resources/companyInfo.csv" -Delimiter ";"
$companyName = $companyInfo.CompanyName #"VintageLiv"
$internalDomain = $companyInfo.DomainInternal.Split(".")
$dc0 = $internalDomain[0] #"vintageliv"
$dc1 = $internalDomain[1] #"intern"
$computers = Import-Csv -Path "99 resources/computers.csv" -Delimiter ";"

foreach ($computer in $computers) {
    if (($computer.Type -eq "DC") -or ($computer.Type -eq "FSrv")) {
        $domainComputer = Get-ADComputer -filter * | Where-Object { $_.Name -eq $computer.Name }
        Move-ADObject -Identity $domainComputer.DistinguishedName `
            -TargetPath "OU=Servers,OU=$($companyname)_Computers,DC=$dc0,DC=$dc1"
    }
    else {
        $target = Get-ADOrganizationalUnit -filter * | Where-Object { $_.Name -eq "$($computer.Department)_Computers" }
        Move-ADObject -Identity "CN=$($computer.Name),CN=Computers,DC=$dc0,DC=$dc1" `
            -TargetPath $target.DistinguishedName
    }
}

Write-Host "... DEMO: ..."
Get-ADcomputer -filter *

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3