Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

$importEmployees = "99 resources/noPlayUsers.csv"
#vi importerer en liste over alle som deltar i prosjektet
$employees = Import-Csv -path $importEmployees -Delimiter ";"
$Password = "Password123"
#endre til skriv inn passord??? Kanskje kanskje ikke
$Password = ConvertTo-SecureString $Password -AsPlainText -Force

foreach ($employee in $employees) {
    $name = $employee.DisplayName.Split(" ")
    $sam = $employee.DisplayName.Split(" ")[0]
    $sam += "Admin"

    "`n"
    Write-Host "... adding admin user ($sam) for $name ..."
    
    New-LocalUser $sam -Password $Password -FullName "$name" -Description "NoPlay admin.acc for $name"
    Add-localgroupmember -Group "administrators" -member $sam
}
#og lager lokale administratorbrukere til oss
#disse brukerne er "NoPlay" som betyr at de IKKE representerer ansatte i selskapet, men oss som er "utenfor" i prosjektet
#Dette lar oss logge hvem som gjør hva på serverne

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3