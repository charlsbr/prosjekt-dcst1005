#Dette må kopieres inn etter du har lukket powershell og åpnet pwsh som administrator
clear
"`n"

$username = Read-Host "Git username"
$email = Read-Host "Git email"

"`n"
Write-Host "...Cloning files from GIT..."

git config --global user.name $username
git config --global user.email $email
New-Item -Path "C:/Git" -Type Directory
Set-Location "C:/Git"
git clone https://gitlab.stud.idi.ntnu.no/charlsbr/prosjekt-dcst1005.git
Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
./"10 mountUsb.ps1"
./"02 initialSetup.ps1"