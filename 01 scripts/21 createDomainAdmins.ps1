Set-Location "C:/Git/prosjekt-dcst1005/01 scripts"
$scriptName = ""
if ($PSCommandPath.Split("\").Length -eq 1) {
    $scriptname = $PSCommandPath.Split("/")[-1]
}
else {
    $scriptname = $PSCommandPath.Split("\")[-1]
}
"`n"
Write-Host "... running $scriptName ..."
#Denne koden skriver til konsollen hvilket script som kjører

#dette scriptet skal gjøre brukerne våre til domene administratorer
#kjøres etter domenet er bygd og de lokale brukerne er laget
#på mgr fra Administrator@domene.navn
$newUsers = get-aduser -filter * | where-Object {
    ($_.name -like "*Admin") -and ($_.name -ne "Admin") 
    #hvis en AD user har Admin i brukernavnet sitt, kan de bli admin
} | Select-Object name

# Legger de spesifiserte brukerne inn i "Domain Admins" gruppen
foreach ($user in $newUsers) {
    #Kilde:
    #https://www.educba.com/powershell-loop-through-array/
    Add-ADGroupMember -Identity "Domain Admins" -Members $user.name

    "`n"
    Write-host '... added' $user.name 'to the global domain admin group ...';
}

"`n"
Write-Host "!!! Please log out of this account and into your personal admin account ..."

"`n"
Write-Host "... finished $scriptName ..."
Start-Sleep -Seconds 0.3